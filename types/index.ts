export interface MarkdownHref {
  link:
    | string
    | {
        text: string
        href: string
      }
    | {
        text: string
        href: string
      }[]
}

interface MarkdownRunMap {
  title: string
  src: string
  padding?: number
}

export interface RunMarkdown {
  slug: string
  markdown: {
    title: string
    date: Date
    location: MarkdownHref
    setter: MarkdownHref
    isCurrent: boolean
    contents: string
    type: string
    distances?: string
    images?: string[]
    iframes?: MarkdownRunMap[]
  }
}

export interface WpPost {
  id: number
  slug: string
  date: string
  date_gmt: string
  title: {
    rendered: string
  }
  excerpt: {
    rendered: string
  }
  content: {
    rendered: string
  }
  _embedded: {
    author: {
      name: string
      id: string
    }[]
    'wp:featuredmedia': {
      id: number
      source_url: string
    }[]
  }
}
