/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-var-requires */
import path, { join } from 'path'
import * as chrono from 'chrono-node'
import chalk from 'chalk'
import jdown from 'jdown'
import { kebabCase, uniq, omit, pickBy } from 'lodash'
import { makePageRoutes } from 'react-static/node'

// Typescript support in static.config.js is not yet supported, but is coming in a future update!

const basePath = 'rs'
const runsDir = 'src/runs'
const allCSV = require('./tmp-json/allCSV.json')
// const allCSVLite = require('./tmp-json/allCSVLite.json')
// const allRaceCSVLite = require('./tmp-json/allRaceCSVLite.json')
const allResultsGroups = require('./tmp-json/allResultByYearRaceLength.json')
const allHandicapGroups = require('./tmp-json/allHandicapByYearRaceLength.json')
const allClubPoints = require('./tmp-json/allClubpointsByYearRaceLength.json')
const allSpeedGroups = require('./tmp-json/allSpeedpointsByYearRaceLength.json')
// https://bendigouniathsclub.org.au/rs/file/csv/2021-kangaroo-flat-falter-10-kangaroo-flat-falter-results-medium
//http://localhost:3000             /rs/file/csv/2021-kangaroo-flat-falter-10-kangaroo-flat-falter-results-medium

const resultYears = allResultsGroups.map(
  (raceGroups) => raceGroups[0][0][0].raceFolder.year
)
const handicapYears = allHandicapGroups.map(
  (raceGroups) => raceGroups[0][0][0].raceFolder.year
)

const raceFolderTypeLengthGroups = require('./tmp-json/raceFolderTypeLengthGroups.json')
const allPost = require('./tmp-json/allPost.json')
const postsPerNewsPage = 3

const allRaceIdWinnersByYearByLength = require('./tmp-json/allRaceIdWinnersByYearByLength.json')

const doingNews = false
const newsRoutes = doingNews
  ? [
      ...allPost.map((post, pix) => ({
        path: `/news/post/${post.slug}`,
        template: 'src/containers/Post',
        getData: () => ({
          post,
          nextSlug: pix < allPost.length - 1 ? allPost[pix + 1].slug : '',
          prevSlug: pix > 0 ? allPost[pix - 1].slug : '',
          newsSlug: `/news/page/${Math.trunc(pix / postsPerNewsPage) + 1}`,
        }),
      })),
      ...makePageRoutes({
        items: allPost,
        pageSize: postsPerNewsPage,
        pageToken: 'page',
        route: {
          path: '/news',
          template: 'src/pages/news',
        },
        decorate: (allPost, i, totalPages) => ({
          // For each page, supply the posts, page and totalPages
          getData: () => ({
            allPost,
            currentPage: i,
            totalPages,
          }),
        }),
      }),
    ]
  : []

export default {
  // eslint-disable-next-line no-undef
  maxThreads: 2,
  entry: path.join(__dirname, 'src', 'index.tsx'),
  devBasePath: basePath,
  basePath,
  getRoutes: async () => {
    console.log(chalk.blue(`Getting race calander files`))
    const allRace = Object.entries(await jdown(runsDir)).map(([key, value]) => {
      // Breaks with tabs in yaml...
      //   grep $'\t' src/runs/*md
      // console.log(
      //   `debug static.config.js: key=${key} kebab=${kebabCase(
      //     value
      //   )} ${JSON.stringify(value, null, ' ')}\n\n`
      // )
      // console.log('DATE', value.date)
      value.date = chrono.parseDate(`${value.date}`)
      if (!value.raceId) {
        value.raceId = kebabCase(key)
      }
      return {
        slug: `/runs/${kebabCase(key)}`,
        markdown: value,
      }
    })
    const currentRaces = allRace
      .filter((r) => r.markdown.isCurrent)
      .sort((a, b) => a.markdown.date - b.markdown.date)
    const restingRaces = allRace
      .filter((r) => !r.markdown.isCurrent)
      .sort((b, a) => b.markdown.date - a.markdown.date)

    const allMember = require('./tmp-json/allMember.json')

    const recentResults = allResultsGroups.flat(1).slice(0, 10)
    const latestHandicapCSV = allCSV.filter((c) => c.buacType === 'HANDICAP')[0]
    const latestHandicapGroup =
      raceFolderTypeLengthGroups[latestHandicapCSV.raceFolder.folder][
        latestHandicapCSV.buacType
      ]
    return [
      {
        path: '/',
        getData: () => ({ currentRaces, recentResults, latestHandicapGroup }),
      },
      {
        path: '/runs',
        getData: () => ({ currentRaces }),
      },
      {
        path: '/runs/resting',
        getData: () => ({ restingRaces }),
      },
      {
        path: '/people',
        getData: () => ({
          allMember: allMember,
          allClub: uniq(allMember.map((m) => m.club)),
        }),
      },

      // Runs pages
      ...allRace.map((r) => ({
        path: r.slug,
        template: 'src/containers/Race',
        getData: () => ({
          race: r,
          winners: allRaceIdWinnersByYearByLength.find(
            (g) =>
              g[0][0].csv.raceFolder.frontMatter.raceId === r.markdown.raceId
          ),
          // currentRaces,
          // restingRaces,
          // raceWinnerByLength: allRaceWinner.filter( (w) => w.raceFolder.frontMatter.raceId === r.markdown.raceId),
        }),
      })),

      // Points. We publish teaser points for a few ( or no ) weeks during year
      // (some years) , and then at the end we publish the final points around
      // the last points scoring race of the year.  Here we send the 'last'
      // points for each year to the summary table
      {
        // Points - club
        path: '/results/club',
        template: 'src/containers/Points',
        getData: () => ({
          pointsByYearByLength: allClubPoints.map((yearGroup) => yearGroup[0]),
          buacType: allClubPoints[0][0][0][0].buacType,
        }),
      },
      {
        // Points - speed
        path: '/results/speed',
        template: 'src/containers/Points',
        getData: () => ({
          pointsByYearByLength: allSpeedGroups.map((yearGroup) => yearGroup[0]),
          buacType: allSpeedGroups[0][0][0][0].buacType,
        }),
      },
      {
        // latest results and index
        path: '/results',
        template: 'src/containers/Results',
        getData: () => ({
          year: allResultsGroups[0][0][0][0].raceFolder.year,
          years: resultYears,
          raceGroupsByLength: allResultsGroups[0],
        }),
      },
      {
        // latest handicaps and index
        path: '/handicaps',
        template: 'src/containers/Results',
        getData: () => ({
          year: allHandicapGroups[0][0][0][0].raceFolder.year,
          years: handicapYears,
          raceGroupsByLength: allHandicapGroups[0],
        }),
      },
      // Year results,
      ...allResultsGroups.map((raceGroupsByLength) => {
        const year = raceGroupsByLength[0][0][0].raceFolder.year
        return {
          path: `results/${year}`,
          template: 'src/containers/Results',
          getData: () => ({
            year,
            years: resultYears,
            raceGroupsByLength,
          }),
        }
      }),
      // Year Handicaps
      ...allHandicapGroups.map((raceGroupsByLength) => {
        const year = raceGroupsByLength[0][0][0].raceFolder.year
        return {
          path: `handicaps/${year}`,
          template: 'src/containers/Results',
          getData: () => ({
            year,
            years: handicapYears,
            raceGroupsByLength,
          }),
        }
      }),

      ...allCSV.map((csv) => {
        csv.slug = join('file', 'csv', csv.slug)
        return {
          path: csv.slug,
          template: 'src/containers/CSV',
          getData: () => ({
            csv,
            typeLengthGroups:
              raceFolderTypeLengthGroups[csv.raceFolder.folder][csv.buacType],
            otherTypeLengthGroups: pickBy(
              omit(
                raceFolderTypeLengthGroups[csv.raceFolder.folder],
                csv.buacType
              ),
              (bTypeCSVs) => bTypeCSVs.length
            ),
          }),
        }
      }),

      ...require('./tmp-json/allMemberResult.json').map(({ member, csvs }) => ({
        // Ben McDermid-> ben-mcdermid NOT ben-mc-dermid
        path: `/person/${kebabCase(member.name.toLowerCase())}`,
        template: 'src/containers/MemberResult',
        getData: () => ({
          member,
          csvs,
        }),
      })),

      ...newsRoutes,
    ]
  },
  plugins: [
    'react-static-plugin-typescript',
    [
      require.resolve('react-static-plugin-source-filesystem'),
      {
        location: path.resolve('./src/pages'),
      },
    ],
    [
      'react-static-plugin-react-router',
      {
        RouterProps: {
          basename: basePath,
        },
      },
    ],
    require.resolve('react-static-plugin-sitemap'),
    [
      'react-static-plugin-favicons',
      {
        inputFile: path.resolve(
          __dirname,
          'public/assets/homepage/bioShield.png'
        ),
      },
    ],
  ],
}
