#!/usr/bin/env bash

echo "brew install lftp"
echo "Have you got webmaster webmaster@bendigouniathsclub.org.au password in ~/.netrc"
lftp -e 'set ssl:verify-certificate false;mirror -R --delete --ignore-time --depth-first --parallel=1 dist rs;bye' ftp.bendigouniathsclub.org.au
