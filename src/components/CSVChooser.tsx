import React from 'react'
import {
  Button,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuGroup,
} from '@chakra-ui/react'
import { ChevronDownIcon } from '@chakra-ui/icons'
import { Link } from 'react-router-dom'

import { BuacCSV } from 'lib/race-folder'
import { startCase } from 'lodash'

export const buacCSVSlug = (csv: BuacCSV): string => `/file/csv/${csv.slug}`

type Props = {
  activeCSV?: BuacCSV
  csvs: BuacCSV[]
  buttonSize?: string
  // TODO when we are not passing Length Groups
  // buttonTitle: string
  // itemLabelFn: any
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const CSVChooser = ({ csvs, activeCSV, buttonSize }: Props) => {
  const buttonSizeUse = buttonSize || 'xs'
  const length = csvs[0].buacLength
  const buttonTitle = ['SHORT', 'MEDIUM', 'LONG'].some((l) => length === l)
    ? startCase(length.toLowerCase())
    : length
  const active = activeCSV && activeCSV.fn == csvs[0].fn
  const activeMenu =
    activeCSV && csvs.find((c) => c.buacLength == activeCSV.buacLength)
  return csvs.length === 1 ? (
    <Link to={buacCSVSlug(csvs[0])}>
      <Button
        size={buttonSizeUse}
        isActive={active}
        variant={active ? 'outline' : 'solid'}
      >
        {buttonTitle}
      </Button>
    </Link>
  ) : (
    <Menu>
      <MenuButton
        variant={activeMenu ? 'outline' : 'solid'}
        as={Button}
        size={buttonSizeUse}
        rightIcon={<ChevronDownIcon />}
      >
        {activeMenu ? activeCSV.csvType : buttonTitle}
      </MenuButton>
      <MenuList>
        <MenuGroup title={buttonTitle}>
          {csvs.map((csv: BuacCSV) => (
            <MenuItem key={csv.slug}>
              <Link to={buacCSVSlug(csv)}>{csv.csvType}</Link>
            </MenuItem>
          ))}
        </MenuGroup>
      </MenuList>
    </Menu>
  )
}
export default CSVChooser
