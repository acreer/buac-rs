import React from 'react'
import {
  Box,
  BoxProps,
  Heading as ChHeading,
  Text as ChText,
  HeadingProps,
  TextProps,
  useColorModeValue,
} from '@chakra-ui/react'

export const Heading = (props: HeadingProps) => (
  <ChHeading
    color={useColorModeValue('gray.700', 'yellow.500')}
    py="3"
    {...props}
  />
)

export const Text = (props: TextProps) => <ChText mb={3} {...props} />

export const BorderBox = (props: BoxProps) => (
  <Box
    borderColor="gray.500"
    borderRadius="lg"
    borderWidth="1px"
    p="2"
    m="2"
    {...props}
  />
)

export const DangerousBox = (props: BoxProps) => (
  <Box
    css={{
      p: {
        marginTop: '0.95em',
        marginBottom: '0.95em',
      },
      li: {
        marginBottom: '0.5em',
        paddingLeft: '2em',
      },
      whiteSpace: 'normal',
      listStyle: 'inside',
    }}
    {...props}
  />
)
