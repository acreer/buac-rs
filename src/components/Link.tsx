import React from 'react'
import { useColorModeValue } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'

export const avURL = 'https://athsvic.org.au/'
export const abURL = 'https://www.athleticsbendigo.org.au/'

import {
  Link as LinkRouter,
  LinkProps as LinkPropsRouter,
} from 'react-router-dom'
import {
  Link as LinkChakra,
  LinkProps as LinkPropsChakra,
} from '@chakra-ui/react'

type CustomProps = {
  hideExternalIcon?: boolean
}
type LinkProps = LinkPropsChakra & LinkPropsRouter & CustomProps

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const Link = ({
  to,
  children,
  hideExternalIcon,
  isExternal: isExternalProp,
  ...rest
}: LinkProps) => {
  const isExternal = isExternalProp || `${to}`.match(/^http/)
  const props: any = {
    ...(isExternal ? {} : { as: LinkRouter }),
    ...(isExternal ? { href: to } : { to }),
    isExternal,
    color: useColorModeValue('teal.500', 'yellow.200'),
    ...rest,
  }
  return (
    <React.Fragment>
      <LinkChakra {...props}>
        <span style={{ display: 'inline-block' }}>
          {children}
          {isExternal && !hideExternalIcon && <ExternalLinkIcon mx={1} />}
        </span>
      </LinkChakra>
    </React.Fragment>
  )
}
