import React, { useState } from 'react'
import {
  Text,
  Button,
  Table,
  Tr,
  Td,
  Tbody,
  Thead,
  Box,
} from '@chakra-ui/react'
import { kebabCase, startCase } from 'lodash'
import { TriangleDownIcon, TriangleUpIcon } from '@chakra-ui/icons'
import { Link } from 'components/Link'

const TableText: React.FC = ({ children, ...rest }) => (
  <Text fontSize="sm" {...rest}>
    {children}
  </Text>
)

const csvSort = (data: any[], key: string, desc: boolean) => {
  if (!key) {
    return data
  }
  const sorted = data.sort((a, b) =>
    a[key].toString().localeCompare(b[key].toString(), 'en', { numeric: true })
  )
  return desc ? sorted.reverse() : sorted
}

type Props = {
  data: any[]
}

const veryImprobableDataName =
  'sfasjflkjfkj lskdjfslkdjfslkdfjslkdjfslkjd sdfksjldfj'

export default function SortedTable({ data: rawData }: Props) {
  const [sortCol, setSortCol] = useState(veryImprobableDataName)
  const [sortDesc, setSortDesc] = useState(false)
  const data = rawData.map((i) => i)

  const cols = Object.keys(data[0]).filter(
    (c: string) => c !== 'member' && c !== 'slug'
  )

  const sortedData =
    sortCol === veryImprobableDataName
      ? rawData
      : csvSort(data, sortCol, sortDesc)

  const handleClick = (col: string) => {
    if (col == sortCol) {
      if (sortDesc) {
        setSortCol(veryImprobableDataName)
      }
      setSortDesc(!sortDesc)
    } else {
      setSortDesc(false)
      setSortCol(col)
    }
  }

  const colStyle = { padding: '5px 10px', textAlign: 'center' as const }
  return (
    <Box style={{ overflowX: 'auto', whiteSpace: 'nowrap' }}>
      <Table>
        <Thead>
          <Tr>
            {cols.map((c: string, cx: number) => (
              <Td key={cx} style={colStyle}>
                <Button
                  {...(c === sortCol
                    ? {
                        rightIcon: sortDesc ? (
                          <TriangleDownIcon />
                        ) : (
                          <TriangleUpIcon />
                        ),
                      }
                    : {})}
                  size="xs"
                  onClick={() => handleClick(c)}
                >
                  {startCase(c)}
                </Button>
              </Td>
            ))}
          </Tr>
        </Thead>
        <Tbody>
          {sortedData.map((row: any) => {
            // See similar in WinnerCellInfo in Race.tsx
            const memberSlug = kebabCase(row.slug)
            return (
              <Tr key={memberSlug}>
                {cols.map((c: string) => (
                  <Td key={c} style={colStyle}>
                    {['first', 'last', 'name'].includes(c) ? (
                      <TableText>
                        <Link to={`/person/${memberSlug}`}>{row[c]}</Link>
                      </TableText>
                    ) : (
                      <TableText>{row[c]}</TableText>
                    )}
                  </Td>
                ))}
              </Tr>
            )
          })}
        </Tbody>
      </Table>
    </Box>
  )
}
