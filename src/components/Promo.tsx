import React from 'react'

import {
  Box,
  Button,
  Text,
  useBreakpointValue,
  Heading,
} from '@chakra-ui/react'
import { Link } from 'components/Link'
export const membershipURL =
  'https://www.eventbrite.com/e/430063801407'
export const halfMaraURL =
  'https://www.eventbrite.com.au/e/bendigo-university-ac-half-marathon-festival-tickets-216238584427'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const JoinOnlineImage = require('public/assets/promotionBanners/uniHalfRunners.png')

type PromoProps = {
  heading?: string
  text?: string
  button?: string
  link?: string
}

export const Promo = ({ heading, text, button, link }: PromoProps) => {
  const outMargin = [2, 10]
  const headingSize = useBreakpointValue({ base: 'sm', md: 'lg' })
  return (
    <Box
      style={{
        border: '1pt solid orange',
        backgroundImage: `url(${JoinOnlineImage})`,
      }}
    >
      <Box pl="2em">
        <Box mt={outMargin} />
        {heading && (
          <Heading color="white" size={headingSize}>
            {heading}
          </Heading>
        )}
        {text && (
          <Text color="white" my={4}>
            {text}
          </Text>
        )}
        {button && (
          <Link hideExternalIcon color="white" to={link}>
            <Button variant="outline" colorScheme="white" mb={outMargin}>
              {button}
            </Button>
          </Link>
        )}
        <Box mb={outMargin} />
      </Box>
    </Box>
  )
}
