import React from 'react'
import { Box, Image } from '@chakra-ui/react'
import { Link } from 'components/Link'

const humeIser = require('public/assets/sponsors/STU01130_Hume-&-Iser-M10-Horiz_Rev_R1.jpg')
const humeIserUrl =
  'https://www.mitre10.com.au/stores/vic/bendigo-hume-iser-mitre-10-2868/'

const Sponsors = () => (
  <Box>
    <Box height={30} my={15} />
    <hr />
    <Box>Bendigo University Athletics Club is proudly sponsored by</Box>
    <Box>
      <Link hideExternalIcon to={humeIserUrl}>
        <Image src={humeIser} maxH="20" />
      </Link>
    </Box>
  </Box>
)

export default Sponsors
