//import _ from 'lodash'
import React from 'react'
import { useBreakpointValue } from '@chakra-ui/react'
import Carousel from 'nuka-carousel'

type BuacCarouselProps = {
  children: React.ReactNode
  slidesToShow?: number
}

export const BuacCarousel = ({
  children,
  slidesToShow = useBreakpointValue({ base: 1, lg: 2 }),
}: BuacCarouselProps) => (
  <Carousel
    slidesToShow={slidesToShow}
    swiping
    autoplay
    renderCenterLeftControls={({ previousSlide }) => (
      <button onClick={previousSlide}>&#10094; </button>
    )}
    renderCenterRightControls={({ nextSlide }) => (
      <button onClick={nextSlide}> &#10095;</button>
    )}
  >
    {children}
  </Carousel>
)

export const PublicityImages = () => {
  const slides = [
    { img: require('public/assets/banners/1580x890/IMG_0961.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_4969.jpg') },
    {
      img: require('public/assets/banners/1580x890/20190727_062300131_iOS.jpg'),
    },
    { img: require('public/assets/banners/1580x890/IMG_3908.jpg') },
    {
      img: require('public/assets/banners/1580x890/20201220_040815419_iOS.jpg'),
    },
    { img: require('public/assets/banners/1580x890/IMG_7704.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_8041.jpg') },
    {
      img: require('public/assets/banners/1580x890/20190727_062551548_iOS.jpg'),
    },
    {
      img: require('public/assets/banners/1580x890/20190727_062136832_iOS.jpg'),
    },
    {
      img: require('public/assets/banners/1580x890/20190727_062706323_iOS.jpg'),
    },
    {
      img: require('public/assets/banners/1580x890/20190421_005329902_iOS.jpg'),
    },
    { img: require('public/assets/banners/1580x890/IMG_9954.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_9611.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_9941.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_9869.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_2937.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_7838.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_5222.jpg') },
    {
      img: require('public/assets/banners/1580x890/20190504_053244383_iOS.jpg'),
    },
    {
      img: require('public/assets/banners/1580x890/20190727_062339251_iOS.jpg'),
    },
    { img: require('public/assets/banners/1580x890/IMG_5123.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_0437.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_8241.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_9743.jpg') },
    { img: require('public/assets/banners/1580x890/IMG_0254.jpg') },
  ]
  const slidesToShow = useBreakpointValue({ base: 1, lg: 2 })
  return (
    <Carousel
      slidesToShow={slidesToShow}
      swiping
      autoplay
      renderCenterLeftControls={({ previousSlide }) => (
        <button onClick={previousSlide}>&#10094; </button>
      )}
      renderCenterRightControls={({ nextSlide }) => (
        <button onClick={nextSlide}> &#10095;</button>
      )}
    >
      {slides.map(({ img }) => (
        <img key={img} src={img} />
      ))}
    </Carousel>
  )
}
