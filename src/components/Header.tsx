/* eslint-disable react/jsx-key */
import React from 'react'
import {
  Box,
  Button,
  Flex,
  Link as LinkChakra,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Spacer,
  Text,
  useDisclosure,
} from '@chakra-ui/react'
import { Link as LinkFunctional, LinkProps } from 'react-router-dom'
import { HamburgerIcon, CloseIcon } from '@chakra-ui/icons'

import { ColorModeSwitcher } from './ColorModeSwitcher'
export const Link = ({ children, to }: LinkProps) => (
  <React.Fragment>
    <LinkChakra color="yellow.500" as={LinkFunctional} to={to}>
      {children}
    </LinkChakra>
  </React.Fragment>
)
/*
TODO Render submenu in mobile...
  <Menu>
    <MenuButton>Items</MenuButton>
    <MenuList>
      <MenuItem>
        <Link to="/about/faq">FAQ</Link>
      </MenuItem>
      <MenuItem>Uniform</MenuItem>
      <MenuItem>Summer Track</MenuItem>
    </MenuList>
  </Menu>,
 */
const links = [
  <Link to="/runs">Runs</Link>,
  <Link to="/results">Results</Link>,
  // <Link to="/news">News</Link>,
  <Link to="/about">About</Link>,
]

export default () => {
  const { isOpen, onToggle } = useDisclosure()

  return (
    <Flex
      as="nav"
      bg="gray.700"
      align="center"
      px={[2, 2, 3, 4]}
      py={[2, 4, 6, 8]}
    >
      <Link to="/">
        <Text display={{ base: 'none', sm: 'block' }} as="b" size="lg">
          Bendigo University Athletics Club
        </Text>
        <Text display={{ base: 'block', sm: 'none' }} as="b">
          Bendigo Uni Aths Club
        </Text>
      </Link>
      <Spacer />
      {links.map((L, lx) => (
        <Box
          display={{ base: 'none', md: 'block' }}
          key={lx}
          mr={lx == links.length - 1 ? 0 : [1, 2, 4, 6]}
        >
          {L}
        </Box>
      ))}
      <ColorModeSwitcher />
      <Box display={{ base: 'block', md: 'none' }}>
        <Menu>
          <MenuButton
            colorScheme="gray"
            color="yellow.500"
            as={Button}
            onClick={onToggle}
          >
            {isOpen ? <CloseIcon color="yellow.500" /> : <HamburgerIcon />}
          </MenuButton>
          <MenuList>
            {links.map((L, lx) => (
              <MenuItem key={lx} mr="0">
                <Box ml="auto">{L}</Box>
              </MenuItem>
            ))}
          </MenuList>
        </Menu>
      </Box>
    </Flex>
  )
}
