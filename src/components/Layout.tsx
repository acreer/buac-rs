import React from 'react'
import { Head } from 'react-static'
import { Box, BoxProps } from '@chakra-ui/react'
import Header from 'components/Header'
import { Heading } from 'components/Chakra'
import { Promo, membershipURL, halfMaraURL } from 'components/Promo'
import { PublicityImages } from 'components/Carousel'
import Sponsors from 'components/Sponsors'
// import {read} from 'gray-matter'

type CustomLayoutProps = {
  joinOnline?: boolean
  promoteHalfMara?: boolean
  promoteUniInvite?: boolean
  promoteAGM?: boolean
  reviewFees?: boolean
  title?: string
  noCarousel?: boolean
}

// const {data:uniInvite} = read('../runs/uni-invite.md')

type Props = BoxProps & CustomLayoutProps
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const Layout = ({
  title,
  children,
  joinOnline,
  reviewFees,
  promoteHalfMara,
  promoteUniInvite,
  promoteAGM,
  noCarousel,
  ...rest
}: Props) => (
  <Box {...rest}>
    <Header />
    {promoteAGM && (
      <Promo
        heading="Club AGM and Presentations"
        text="One Tree Hotel. September 17 2022, 4:30pm"
        button="We need to know numbers. Please Book Free Ticket"
        link="https://www.eventbrite.com.au/e/2022-bendigo-university-ac-cross-country-presentation-party-agm-tickets-396321306697"
      />
    )}
    {reviewFees && (
      <Promo
        heading="Online Membership"
        text="From October 1 each year"
        button="Review Membership Fees"
        link="/about/fees"
      />
    )}
    {joinOnline && (
      <Promo
        heading="Online Membership"
        text="From October 1 each year"
        button="Join or Renew Now"
        link={membershipURL}
      />
    )}
    {promoteUniInvite && (
      <Promo
        heading="Uni Invite - Start at Latrobe Uni"
        text="Saturday April 29 2023"
        button="Enter Online Now"
        link={'https://my.raceresult.com/238508/registration'}
      />
    )}
    {promoteHalfMara && (
      <Promo
        heading="Half Marathon Festival"
        text="7 am December 19 2021"
        button="Enter Online NOW"
        link={halfMaraURL}
      />
    )}
    {!noCarousel && <PublicityImages />}
    <Box px={[2, 4, 6, 8]}>
      {title && (
        <React.Fragment>
          <Head>
            <title>{title}</title>
          </Head>
          <Heading>{title}</Heading>
        </React.Fragment>
      )}
      {children}
      <Sponsors />
    </Box>
  </Box>
)

export default Layout
