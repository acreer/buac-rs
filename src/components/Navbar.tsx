import React from 'react'
import { Link as LinkFunctional, LinkProps } from 'react-router-dom'
import { ColorModeSwitcher } from './ColorModeSwitcher'
import {
  Box,
  Flex,
  TextProps,
  Link as LinkChakra,
  Text,
  useDisclosure,
} from '@chakra-ui/react'
import { HamburgerIcon, CloseIcon } from '@chakra-ui/icons'

const Link = ({ children, to }: LinkProps) => (
  <React.Fragment>
    <LinkChakra color="yellow.500" as={LinkFunctional} to={to}>
      {children}
    </LinkChakra>
  </React.Fragment>
)

type MIProps = {
  isLast?: boolean
}

const MenuItems = ({
  children,
  to,
  isLast,
  ...rest
}: LinkProps & TextProps & MIProps) => {
  return (
    <Text
      mb={{ base: isLast ? 0 : 8, sm: 0 }}
      mr={{ base: 0, sm: isLast ? 0 : 8 }}
      display="block"
      {...rest}
    >
      <Link to={to}>{children}</Link>
    </Text>
  )
}

const Navbar = () => {
  const {
    isOpen,
    // onOpen, onClose,
    onToggle,
  } = useDisclosure()
  return (
    <Flex
      as="nav"
      p="8"
      mb="8"
      bg="gray.800"
      justify="space-between"
      align="center"
    >
      <Text as="b" size="lg">
        <Link to="/">Bendigo University Athletics Club</Link>
      </Text>
      <Box
        display={{ base: isOpen ? 'block' : 'none', md: 'block' }}
        flexBasis={{ base: '100%', md: 'auto' }}
      >
        <Flex
          align={['center', 'center', 'center', 'center']}
          justify={['center', 'space-between', 'flex-end', 'flex-end']}
          direction={['column', 'column', 'row', 'row']}
          pt={[4, 4, 0, 0]}
        >
          <MenuItems to="/runs">Runs</MenuItems>
          <MenuItems to="/results">Results</MenuItems>
          <MenuItems to="/about" isLast>
            About
          </MenuItems>
        </Flex>
      </Box>
      <Flex alignItems="center" color="yellow.500">
        <Box onClick={onToggle}>
          {isOpen ? <CloseIcon /> : <HamburgerIcon />}
        </Box>
        <ColorModeSwitcher />
      </Flex>
    </Flex>
  )
}

export default Navbar
