import React from 'react'
import { Link } from 'components/Link'
const MarkdownLink = ({ link }: { link: any }) => {
  if (typeof link === 'string') {
    return <span>{link}</span>
  }

  if (Array.isArray(link)) {
    return (
      <span>
        {link.map((l: any, idx: number, arr: any[]) => {
          const divider =
            idx < arr.length - 2 ? ', ' : idx < arr.length - 1 ? ' & ' : null
          return (
            <span key={idx}>
              <MarkdownLink link={l} />
              {divider}
            </span>
          )
        })}
      </span>
    )
  } else {
    return link.href ? (
      <Link to={link.href}>{link.text}</Link>
    ) : (
      <MarkdownLink link={link.text} />
    )
  }
}

export default MarkdownLink
