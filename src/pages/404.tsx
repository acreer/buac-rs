/* eslint-disable react/display-name */
import React from 'react'
import { Text } from '@chakra-ui/react'
import { BorderBox, Heading } from 'components/Chakra'
import Layout from 'components/Layout'
import { MailRun } from 'pages/about'

export default () => (
  <Layout>
    <BorderBox>
      <Heading>Oh No, this page seems to be missing</Heading>
      <Text>
        Perhaps there is too much running and not enough link checking on our
        part
      </Text>
      <Text>Perhaps the wrong address was put into the browser</Text>
      <Text>
        If you used link on this site to get here <MailRun /> would like to know
      </Text>
    </BorderBox>
  </Layout>
)
