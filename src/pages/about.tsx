import React from 'react'
import Layout from 'components/Layout'
import { Text, Heading } from 'components/Chakra'
import { Link } from 'components/Link'
import { memberHandbookPDF } from './about/faq'

export const MailPresident = () => (
  <Link isExternal to="mailto:president@bendigouniathsclub.org.au">
    The President, Ross Douglas
  </Link>
)
export const MailTreasurer = () => (
  <Link isExternal to="mailto:treasurer@bendigouniathsclub.org.au">
    The Treasurer, Ben McDermid
  </Link>
)

export const MailInfo = () => (
  <Link isExternal to="mailto:info@bendigouniathsclub.org.au">
    The Secretary, David Lonsdale
  </Link>
)

export const MailRun = () => (
  <Link isExternal to="mailto:run@bendigouniathsclub.org.au">
    The Runners, Andrew Creer
  </Link>
)

export const MailSummerCaptain = () => (
  <Link isExternal to="mailto:summercaptain@bendigouniathsclub.org.au">
    The Summer Captain
  </Link>
)

export const LinkAthsBendigo = () => (
  <Link isExternal to="https://www.athleticsbendigo.org.au/">
    Athletics Bendigo
  </Link>
)

export const LinkAthsVic = () => (
  <Link isExternal to="https://athsvic.org.au/">
    Athletics Victoria
  </Link>
)

// eslint-disable-next-line react/display-name
export default () => (
  <Layout reviewFees title="About the Club">
    <Text>
      We are Bendigo University Athletics Club (BUAC). Established in 1967, we
      are Bendigo’s biggest running club. <MailPresident /> is always ready to
      help new members settle in and enjoy their running.
    </Text>
    <Text>
      The <Link to="about/summer-track">Summer Track Season</Link> gets under
      way around the time of Daylight Savings in October. Many Members compete
      in these events.
    </Text>
    <Text>
      In the cooler months, from March to September, we host a{' '}
      <Link to="/runs">program</Link> of timed, family-friendly running races in
      the bushland surrounding Bendigo. Races are on most Saturdays from 2pm
      over 4 distances. 500m for the littlies, 1km, 3km and our longer race
      averages 7km. Our winter handicap system enables every participant the
      chance to win the race. (Yes, that includes you.)
    </Text>
    <Text>
      Look great and feel fantastic in our official{' '}
      <Link to="/about/uniform/">Club Uniform</Link>
    </Text>
    <Heading>Want to know more?</Heading>
    <Text>
      Check out the <Link to="/about/faq">Frequently asked questions</Link> or
      read the <Link to={memberHandbookPDF}>Member Handboook</Link>.
    </Text>
    <Heading>Try running with us</Heading>
    <Text>New members are always welcome.</Text>
    <Text>
      In the Summer from October to March, the club is part of a larger more
      formal <LinkAthsBendigo /> track competition. If you contact the the
      president arrangements can be quickly made to get you running in a uni
      singlet.
    </Text>
    <Text>
      In winter your first run is free. Our club races are a very informal
      affairs, just come along to one of our runs, bring your running gear and
      any family or friends who might also be interested. Check the{' '}
      <Link to="/runs">calandar</Link> for event details and turn up on the day.
    </Text>
  </Layout>
)
