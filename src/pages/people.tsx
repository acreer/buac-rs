import { Button } from '@chakra-ui/button'
import { Checkbox } from '@chakra-ui/checkbox'
import { HStack } from '@chakra-ui/layout'
import Layout from 'components/Layout'
import { Link } from 'components/Link'
import { BuacMember, Club } from 'lib/members'
import { kebabCase } from 'lodash'
import React, { useState } from 'react'
import { useRouteData } from 'react-static'

type Data = {
  allMember: BuacMember[]
  allClub: string[]
}
// static.config.js
// Ben McDermid-> ben-mcdermid NOT ben-mc-dermid
// path: `/person/${kebabCase(member.name.toLowerCase())}`,
const memberSlug = (member: BuacMember) =>
  `/person/${kebabCase(member.name.toLocaleLowerCase())}`

const clubColors = (club: Club) => {
  switch (club) {
    case 'HARRIERS':
      return 'green'
    case 'BUAC':
      return 'yellow'
    default:
      return 'pink.500'
  }
}

const clubList = {
  BUAC: 'Uni',
  HARRIERS: 'Harriers',
  'SOUTH BENDIGO': 'South Bendigo',
  EAGLEHAWK: 'Eaglehawk',
  OTHER: 'Other',
}

const clubFiltered = (member: BuacMember, clubFilter: unknown): boolean => {
  console.log(clubFilter)
  switch (member.club) {
    case 'BUAC':
    case 'HARRIERS':
    case 'EAGLEHAWK':
    case 'SOUTH BENDIGO':
      return true
    //return clubFilter[member.club.toString()]
    default:
      return false
    //return clubFilter['OTHER']
  }
}

export default (): JSX.Element => {
  const { allMember, allClub }: Data = useRouteData()

  console.log(allClub)
  const [clubFilter, setClubFilter] = useState({
    BUAC: false,
    HARRIERS: true,
    'SOUTH BENDIGO': false,
    EAGLEHAWK: false,
    OTHER: true,
  })

  const handleClubChoice = (e: React.ChangeEvent<HTMLInputElement>) =>
    setClubFilter({ ...clubFilter, [e.target.name]: e.target.checked })

  const clubMembers = allMember.filter((m) => clubFiltered(m, clubFilter))
  const perPage = Math.max(clubMembers.length, 40)
  const pages = Math.ceil(clubMembers.length / perPage)
  const [page, setPage] = useState(1)
  const safePage = Math.min(pages, page)
  const shortMemberPage = clubMembers.slice(
    (safePage - 1) * perPage,
    safePage * perPage
  )
  return (
    <Layout title="All the People">
      <HStack>
        {Object.entries(clubList).map(([k, v]) => (
          <Checkbox key={k} name={k} onChange={handleClubChoice}>
            {v}
          </Checkbox>
        ))}
      </HStack>
      <Button onClick={() => setPage(safePage - 1)}>Previous</Button>
      <Button onClick={() => setPage(safePage + 1)}>Next</Button>
      {shortMemberPage.map((member) => {
        const slug = memberSlug(member)
        const color = clubColors(member.club)
        return (
          <Link key={slug} to={slug}>
            <Button colorScheme={color}>{member.name}</Button>
          </Link>
        )
      })}
    </Layout>
  )
}
