import React from 'react'
import { useRouteData } from 'react-static'
import {
  Box,
  Button,
  HStack,
  Image,
  Stack,
  Text,
  useColorModeValue,
} from '@chakra-ui/react'

import { DangerousBox } from 'components/Chakra'
//import { WpPost } from '../../types/index'
import { WP_REST_API_Post, WP_REST_API_Media } from 'wp-types'
import Layout from '../components/Layout'
import { Link } from 'components/Link'
import { Heading } from 'components/Chakra'
import { isoDateTime } from 'pages/runs'

type Post = {
  post: WP_REST_API_Post
}

export const AuthorDate = ({ post }: Post) => {
  const textProps = { colorScheme: 'teal', size: 'sm' }
  return (
    <HStack spacing="10">
      <Text {...textProps}>
        Author :{' '}
        {post._embedded.author.map(({ name }) => (
          <React.Fragment key={name}>{name}</React.Fragment>
        ))}{' '}
      </Text>
      <Text {...textProps}>
        Last Updated : {isoDateTime(post.date).toFormat('DD')}
      </Text>
    </HStack>
  )
}

export const PostHeading = ({ post }: Post) => (
  <Heading
    color={useColorModeValue('yellow.600', 'yellow.500')}
    style={{ textDecorationColor: 'yellow.500' }}
  >
    <DangerousBox dangerouslySetInnerHTML={{ __html: post.title.rendered }} />
  </Heading>
)

type PageButtonProps = {
  currentPage: number
  totalPages: number
}

const PageButtons = ({ currentPage, totalPages }: PageButtonProps) => {
  const nextDisabled = currentPage > totalPages - 1
  return (
    <HStack>
      <Link to={`/news/page/${currentPage - 1}`}>
        <Button disabled={currentPage < 2}>Previous</Button>
      </Link>
      <Link to={`/news/page/${currentPage + 1}`}>
        <Button disabled={nextDisabled}>Next</Button>
      </Link>
    </HStack>
  )
}

// eslint-disable-next-line react/display-name

export const News = () => {
  const { allPost, currentPage, totalPages } = useRouteData()
  const firstPost = allPost[0]
  const lastPost = allPost[allPost.length - 1]
  const title = `${isoDateTime(firstPost.date).toFormat('DD')} - ${isoDateTime(
    lastPost.date
  ).toFormat('DD')}`
  return (
    <Layout title={title}>
      <React.Fragment>
        <div>
          <script
            src="https://bendigouniathsclub.us17.list-manage.com/generate-js/?u=30247a265d32468a4848fb3f2&fid=9562&show=10"
            type="text/javascript"
          ></script>
        </div>
      </React.Fragment>
      <PageButtons {...{ totalPages, currentPage }} />
      <Text as="small">
        {`News page ${currentPage} of ${totalPages}`}
        {isoDateTime(firstPost.date).toFormat('DD')} -{' '}
        {isoDateTime(lastPost.date).toFormat('DD')}
      </Text>
      {allPost.map((post: WP_REST_API_Post, pix: number) => {
        const featuredMedias: WP_REST_API_Media[] = post._embedded[
          'wp:featuredmedia'
        ] as WP_REST_API_Media[]
        const featured = featuredMedias ? (
          <Image
            boxSize="400px"
            objectFit="contain"
            src={featuredMedias[0].source_url}
          />
        ) : null
        const postURL = `/news/post/${post.slug}`

        return (
          <React.Fragment key={post.id}>
            <Box
              borderWidth="1px"
              borderColor="gray.500"
              borderRadius="lg"
              m="2"
              p="2"
            >
              <Box pb="2">
                <Link to={postURL}>
                  <PostHeading post={post} />
                </Link>
              </Box>
              <Image src="nowhere" />
              <AuthorDate post={post} />
              <Box pt="2">
                <Stack direction={['column-reverse', 'row']}>
                  <Box>
                    <DangerousBox
                      dangerouslySetInnerHTML={{
                        __html: post.excerpt.rendered,
                      }}
                    />
                    <Link to={postURL}>
                      <Text as="small">Read More</Text>
                    </Link>
                  </Box>
                  {featured}
                </Stack>
              </Box>
            </Box>
            {pix % 3 === 2 && <PageButtons {...{ totalPages, currentPage }} />}
          </React.Fragment>
        )
      })}
    </Layout>
  )
}

// See doingNews in static.config.js
// export default News
