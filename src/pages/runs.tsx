import React, { useState } from 'react'
import { useRouteData } from 'react-static'
import { Link } from 'components/Link'
import { DateTime } from 'luxon'
import { RunMarkdown } from 'types/index'
import Layout from 'components/Layout'
import { partitionList } from 'lib/utils'
import {
  Box,
  Button,
  Text,
  HStack,
  Stack,
  StackDivider,
  VStack,
  Checkbox,
  useBreakpointValue,
} from '@chakra-ui/react'
import { RunTypes } from 'lib/enums'
import MarkdownLink from 'components/MarkdownLink'

import { BorderBox, Heading } from 'components/Chakra'
import { BuacCSV } from 'lib/race-folder'

export const raceColors = (race: string) => {
  switch (race) {
    case RunTypes.ab:
      return 'green.500'
    case RunTypes.av:
      return 'blue.500'
    case RunTypes.club:
      return 'yellow.500'
    case RunTypes.other:
      return 'pink.500'
  }
}

export const mdRaceColor = (r: RunMarkdown): string =>
  raceColors(r.markdown.type)

export const prettyRaceType = (r: string) => {
  switch (r) {
    case RunTypes.ab:
      return 'Athletics Bendigo'
    case RunTypes.av:
      return 'Athletics Victoria'
    case RunTypes.club:
      return 'Bendgo University Athletics Club'
    case RunTypes.other:
      return 'Other'
  }
}

export const defDateFormat = 'DDDD'

export const csvDateTime = (c: BuacCSV): DateTime =>
  DateTime.fromRFC2822(c.raceFolder.frontMatter.date.toString())

export const raceDateTime = (race: RunMarkdown): DateTime =>
  DateTime.fromISO(race.markdown.date.toString())

// TODO refactor messy date stuff.
export const isoDateTime = (date: unknown): DateTime =>
  DateTime.fromISO(date.toString())

export const raceDateFormat = (
  race: RunMarkdown,
  fmt = defDateFormat
): string => {
  const date = raceDateTime(race)
  // const seasonStart = DateTime.local(2023, 2, 25, 8, 30)
  // const prov = date > seasonStart ? 'PROVISIONALLY ' : ''
  const prov  = ''
  return `${prov}${date.toFormat(fmt)}`
}

type RProps = {
  race: RunMarkdown
}

const RaceItem = ({ race }: RProps) => (
  <Box>
    <Link to={race.slug}>
      <Heading size="md" color={mdRaceColor(race)}>
        {race.markdown.title}
      </Heading>
    </Link>
    <Stack direction={['column', 'row']}>
      <Text>Date {raceDateFormat(race)}</Text>
      <Box pr="2em">
        Course Marker <MarkdownLink link={race.markdown.setter} />
      </Box>
      <Box>
        Location <MarkdownLink link={race.markdown.location} />
      </Box>
    </Stack>
  </Box>
)

const RaceWeek = ({ races }: { races: RunMarkdown[] }) => {
  const days = Math.ceil(
    raceDateTime(races[0]).diff(DateTime.local(), 'days').toObject().days
  )
  const timeString =
    days === 0
      ? ', On TODAY'
      : days < 0
      ? `, was ${-days} days ago`
      : `, in ${days} days time`
  return (
    <BorderBox>
      <Stack divider={<StackDivider borderColor="gray.200" />}>
        <Text as="small" color="grey">
          Week of {raceDateFormat(races[0])}
          {timeString}
        </Text>
        {races.map((r: RunMarkdown) => (
          <RaceItem key={r.slug} race={r} />
        ))}
      </Stack>
    </BorderBox>
  )
}

const NoneSelected = () => (
  <React.Fragment>
    <Heading size="sm">None Selected</Heading>
    <Text>Select at least one above</Text>
  </React.Fragment>
)

export const SummerAthsBendigo = () => {
  const now = DateTime.local()
  if (now.month > 9 || now.month < 4)
    return (
      <React.Fragment>
        <Box my={2}>
          <Text>
            In the warmer months there is plenty happening at the track.
          </Text>
          <Text>
            See the{' '}
            <Link to="https://www.athleticsbendigo.org.au/events-calendar/">
              Athletics Bendigo Events Page
            </Link>
          </Text>
        </Box>
      </React.Fragment>
    )
  return null
}

type Data = {
  currentRaces: RunMarkdown[]
}

export default (): JSX.Element => {
  const { currentRaces }: Data = useRouteData()
  const [raceTypeFilter, setRaceTypeFilter] = useState({
    [RunTypes.club]: true,
    [RunTypes.ab]: true,
    [RunTypes.av]: false,
    [RunTypes.other.toString()]: false,
  })
  const [showPast, setShowPast] = useState(false)

  const handleRaceTypeFilter = (e: React.ChangeEvent<HTMLInputElement>) =>
    setRaceTypeFilter({ ...raceTypeFilter, [e.target.name]: e.target.checked })

  const now = DateTime.local()

  const typeFiltered = currentRaces.filter(
    (r) => raceTypeFilter[r.markdown.type]
  )
  const currentByWeek: RunMarkdown[][] = partitionList(
    typeFiltered.filter((r) => showPast || raceDateTime(r) > now),
    (r: RunMarkdown) => raceDateTime(r).plus({ days: 2 }).toFormat('yyyy-WW')
  )

  const hasPast = typeFiltered.some((r) => raceDateTime(r) < now)

  type Current = {
    group: RunMarkdown[]
    diff: number
  }
  const closestRunWeek = currentByWeek.reduce(
    (c: Current, i: RunMarkdown[]) => {
      const diff = Math.abs(
        DateTime.fromISO(i[0].markdown.date.toString()).valueOf() -
          now.valueOf()
      )
      if (!c.group.length) return { group: i, diff }
      if (c.diff < diff) return c
      return { group: i, diff }
    },
    { group: [], diff: 0 }
  )
  const scrollRef = React.createRef<HTMLDivElement>()

  return (
    <Layout title="Club Run Program">
      <SummerAthsBendigo />
      <Text>
        Schedule of races, organised by week, colour coded by organiser. There
        are also informal{' '}
        <Link to="/about/regular-runs">regular weekly runs</Link> and runs that
        are <Link to="/runs/resting">resting</Link>
      </Text>

      <HStack p={2}>
        {Object.values(RunTypes).map((rt: string) => (
          <Checkbox
            key={rt}
            size={useBreakpointValue(['sm', 'md'])}
            color={raceColors(rt)}
            name={rt}
            isChecked={raceTypeFilter[rt]}
            onChange={handleRaceTypeFilter}
          >
            {prettyRaceType(rt)}
          </Checkbox>
        ))}
      </HStack>
      <HStack display={hasPast ? 'contents' : 'none'}>
        <Checkbox
          p={2}
          size={useBreakpointValue(['sm', 'md'])}
          onChange={() => setShowPast(!showPast)}
          isChecked={showPast}
        >
          Show past races
        </Checkbox>
        {hasPast && showPast && closestRunWeek.group.length > 0 && (
          <React.Fragment>
            <Text as="span"> Jump to current week</Text>
            <Button
              colorScheme="yellow"
              size="sm"
              onClick={() =>
                scrollRef.current.scrollIntoView({ behavior: 'smooth' })
              }
            >
              {raceDateFormat(closestRunWeek.group[0])}
            </Button>
          </React.Fragment>
        )}
      </HStack>
      <Box>
        {currentByWeek.length === 0 && <NoneSelected />}
        <VStack>
          {currentByWeek.map((races: RunMarkdown[]) => (
            <Box
              ref={races == closestRunWeek.group ? scrollRef : null}
              w="100%"
              key={races[0].slug}
            >
              <RaceWeek races={races} />
            </Box>
          ))}
        </VStack>
      </Box>
    </Layout>
  )
}
