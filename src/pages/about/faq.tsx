import React from 'react'
import Layout from 'components/Layout'
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  BoxProps,
  Box,
  Text,
  //ChakraComponent,
} from '@chakra-ui/react'
import { Heading } from 'components/Chakra'
import { Link } from 'components/Link'
import { membershipURL } from 'components/Promo'
import { PointsButton } from 'containers/Points'
import { BuacTypes } from 'lib/enums'

export const memberHandbookPDF =
  'https://bendigouniathsclub.org.au/blog/wp-content/uploads/2021/02/BUAC-Member-Handbook.pdf'

type FaqProps = { faqTitle: React.ReactNode }

const Faq = ({ faqTitle, children, ...props }: BoxProps & FaqProps) => (
  <AccordionItem {...props}>
    <AccordionButton>
      <Box flex="1" textAlign="left">
        {faqTitle}
      </Box>
      <AccordionIcon></AccordionIcon>
    </AccordionButton>
    <AccordionPanel>{children}</AccordionPanel>
  </AccordionItem>
)

export default () => (
  <Layout>
    <Heading>Frequently Asked Questions</Heading>
    <Accordion allowMultiple defaultIndex={[0]}>
      <Faq faqTitle={<Text>Club Handbook</Text>}>
        <Text>
          Yes its <Link to={memberHandbookPDF}>here</Link>
        </Text>
      </Faq>
      <Faq faqTitle="Is there a weekly club email update">
        Yes. And you can subscribe by adding your email addess and name{' '}
        <Link to="email-list">here</Link>
      </Faq>
      <Faq faqTitle="How do I become a member?">
        Review the <Link to="/about/fees/">Fee Structure</Link> and use the{' '}
        <Link to={membershipURL}>Online Registration System</Link>
      </Faq>
      <Faq faqTitle="Do I have to register with Athletics Victoria?">
        Only if you think you may compete in AV events in either the winter XCR
        program or the summer track season. If you join AV and nominate Uni as
        your club, we will reimburse you for your club membership.
      </Faq>
      <Faq faqTitle="Do I have to become a club member?">
        No, you can choose to pay a $10/race casual race fee. This is a good
        option if you think you may only make it to a few races in the season.
        If you are considering joining, then your first race is free. Casual run
        payments will contribute to the cost of membership, but a completed
        membership form (online or paper) is required for club membership.
      </Faq>
      <Faq faqTitle="What is the routine at a club run?">
        Head to the registration table to get your name marked off by the race
        captain. The captain will give you your start time. If you’re a new
        runner, the captain will write in your name, help you with an estimated
        handicap, and issue a bib or tag. Warm up before your start time with a
        short run and some stretches. Listen to the course description by the
        course marker. Wait for the starter to call you to the line and count
        you down to your start. Follow the on course signs to the finish.
        Recover with a warm down, sretches and a cool drink. Enjoy some
        afternoon tea with the other runners and presentation of ribbons for the
        day’s races.
      </Faq>
      <Faq faqTitle="When does my run start?">
        The long run starts at 2pm. The 1 km run starts after the last runner in
        the long run finishes (usually around 3:15pm). The intermediate race is
        the last event on the bill after the finish of the intermediate race
        (around 3:30pm).
      </Faq>
      <Faq faqTitle="What do race winners get?">
        <Text>
          Ribbons are awarded to 1st, 2nd and 3rd placegetters each race.
        </Text>
        <Text>
          The first three place getters in the club champion award in each
          distance category are awarded trophies or medals at the end of the
          year. The speed champion in the long distance event also wins a
          trophy.
          <Box>
            <PointsButton buacType={BuacTypes.clubPoints.toString()} />
            <PointsButton buacType={BuacTypes.speedPoints.toString()} />
          </Box>
        </Text>
        <Text>
          The President’s award goes to the club member who is deemed to have
          made the biggest contribution to the club for the year.
        </Text>
      </Faq>
      <Faq faqTitle="How is my handicap calculated?">
        <Text>
          New members are asked for an estimate of their per km pace at their
          first race. This is used to calculate the start time but the first
          race for a runner is not counted in the club champion award. The
          actual per km pace recorded will help the handicapper establish a more
          accurate handicap for the next race. Each race will help identify
          individual trends which will suggest what adjustment to make on the
          handicapper’s sheet.
        </Text>
        <Text>
          Ribbon winners are normally handicapped more rigorously than others
          for the following race. club champs history current handicaps
        </Text>
      </Faq>
      <Faq faqTitle="How are Championship Points calculated?">
        Check out the Scoring System for how points are allocated in both the
        Club Championship and the Speed Championship.
        <Box>
          <PointsButton buacType={BuacTypes.clubPoints.toString()} />
          <PointsButton buacType={BuacTypes.speedPoints.toString()} />
        </Box>
      </Faq>
      <Faq faqTitle="What should I bring to a race?">
        Water and cordial is available but bring an electrolyte drink if you
        prefer.
        <Text>
          If you intend to stay for afternoon tea, bring something to share like
          a packet of biscuits, cake or slice.
        </Text>
        <Text>Home baked goodies are much appreciated!</Text>
        <Text>
          Tea bags and instant coffee are available if you bring a thermos of
          hot water.
        </Text>
      </Faq>
      <Faq faqTitle="What should I wear?">
        <Text>
          Some days can be quite cold so make sure you have tracksuit pants and
          a warm top to wear before and after your run.
        </Text>
        <Text>
          Club hoodies are an excellent way to stay warm. The club encourages
          you to wear a club singlet or T-shirt but we won’t stop you running if
          you haven’t got one.
        </Text>
        <Text>
          Your running shoes should be suited to cross country terrain.
        </Text>
      </Faq>
      <Faq faqTitle="What are the track surfaces like?">
        Most courses are undulating dirt tracks but some include road running on
        bitumen. Be prepared for eroded, rocky surfaces and wet weather
        conditions. While all courses are safe, most include hills and some
        concentration is required to avoid tripping. Road crossings are kept to
        a minimum but are a part of some long courses.
      </Faq>
      <Faq faqTitle="What if I break down or get injured during a run?">
        Injuries or illness can sometimes happen during a run. If you can’t make
        it back to the start/finish line, sit tight and wait - the club will
        send help after the race is over. If other runners don’t notify the
        timekeeper of your situation, your absence will be noted at the finish
        line. If you make it back under your own steam you can notify the timer
        to put you down as a DNF (did not finish) so your handicap is not
        affected.
      </Faq>
      <Faq faqTitle="Want to do some training during the week?">
        Great! There’s a host of informal and formal weekly running training
        groups. No matter what your running passion or level of ability is there
        will be like-minded people for you to link up with in our club
        community. Most are free or at low cost.
      </Faq>
      <Faq faqTitle="What are my responsibilies as a club member?">
        As with any sporting club, there are tasks that need doing from time to
        time. Each run needs a race captain, a timekeeper and a course marker.
        Volunteers are sometimes needed for marshalling duties at Athletic
        Victoria or Athletics Bendigo events. The University Invitation relies
        on our members to pitch in and help out for registrations, BBQ and the
        afternoon tea stall. Each week, we need somebody to simply take home the
        coffee cups and wash them for the following race. And of course, the
        club committee needs volunteers to fill places at the beginning of each
        year. Responsibilities are not huge or compulsory but sharing the load
        ensures that everybody can enjoy their involvement, help improve the
        club, and encourage more members. Talk to a club committee member about
        how you can be involved.
      </Faq>
      <Faq faqTitle="What club policy documents have been created.">
        Check out the <Link to="/about/policy">Policies</Link> Page
      </Faq>
    </Accordion>
  </Layout>
)
