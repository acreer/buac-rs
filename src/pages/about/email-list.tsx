import React from 'react'
import Layout from 'components/Layout'
import { Text } from 'components/Chakra'
import { Link } from 'components/Link'
import { membershipURL } from 'components/Promo'
import { MailInfo } from 'pages/about'

export default () => (
  <Layout title="Sign up for weekly email">
    <Text>
      The Official BUAC Mailing list is generally used once a week to send
      updates and news to club members and intersted others.
    </Text>
    <Text>
      If you do not currently recieve the weekly updates, you can remedy this
      situation by contacting <MailInfo />
    </Text>
    <Text>
      Please note that existing members and those that sign up using the{' '}
      <Link to={membershipURL}>online membership system</Link> will be
      subscribed to the email list as a matter of course.
    </Text>
    <Text>NOTE each email contains an UNSUBSCRIBE link. receive.</Text>
  </Layout>
)
