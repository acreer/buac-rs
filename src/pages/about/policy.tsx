import React from 'react'
import Layout from 'components/Layout'
import { Text, Box, UnorderedList, ListItem } from '@chakra-ui/react'
import { avURL, Link } from 'components/Link'

// The PDFs live on wordpress.  We download them BUT need to require them so
// that webpack will bundle them. Dont want to commit them in the repo.
//
// Run yarn prepare:policy|grep const|pbcopy
// and check we have the files here

// Begin auto generated code
const BUACCommunicationsPlan = require('public/assets/from-wordpress/policy/BUACCommunicationsPlan.pdf')
const BUACConstitution = require('public/assets/from-wordpress/policy/BUACConstitution.pdf')
const BUACInclusionPolicy = require('public/assets/from-wordpress/policy/BUACInclusionPolicy.pdf')
const BUACLifeMemberGuidelines = require('public/assets/from-wordpress/policy/BUACLifeMemberGuidelines.pdf')
const BUACMemberHandbook = require('public/assets/from-wordpress/policy/BUACMemberHandbook.pdf')
const BUACOperatingProceduresandPoliciesV2 = require('public/assets/from-wordpress/policy/BUACOperatingProceduresandPoliciesV2.pdf')
const BUACVolunteerGuidelines = require('public/assets/from-wordpress/policy/BUACVolunteerGuidelines.pdf')
// END auto generated code
//

type PIProps = { to: any; children: React.ReactNode }
const PolicyItem = ({ to, children }: PIProps) => (
  <ListItem>
    <Link isExternal to={to}>
      {children}
    </Link>
  </ListItem>
)

export const Policies = () => (
  <Box>
    <UnorderedList>
      <PolicyItem to={BUACConstitution}>Constitution</PolicyItem>
      <PolicyItem to={BUACCommunicationsPlan}>Communication Plan</PolicyItem>
      <PolicyItem to={BUACOperatingProceduresandPoliciesV2}>
        Operating Procedure
      </PolicyItem>
      <PolicyItem to={BUACInclusionPolicy}>Inclusion Policy</PolicyItem>
      <PolicyItem to={BUACLifeMemberGuidelines}>Life Membership</PolicyItem>
      <PolicyItem to={BUACMemberHandbook}>Member Handbook</PolicyItem>
      <PolicyItem to={BUACVolunteerGuidelines}>Volunteer Guidelines</PolicyItem>
    </UnorderedList>
  </Box>
)

export default () => (
  <Layout title="BUAC Policy">
    <Text>
      As an Incorporated Body, and an <Link to={avURL}>Athletics Victoria</Link>{' '}
      affiliated club BUAC has a number of policies and operating guidelines.
    </Text>
    <Policies />
  </Layout>
)
