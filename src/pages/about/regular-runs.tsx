import React from 'react'
import Layout from 'components/Layout'
import { BorderBox, Heading, Text } from 'components/Chakra'
import { Link } from 'components/Link'
import { BoxProps } from '@chakra-ui/layout'

const dayHeadingSize = 'md'
const runHeadingSize = 'sm'

const Gazelles = ({ children }: BoxProps) => (
  <BorderBox>
    <Heading size={runHeadingSize}>Gazelles Running Group for Women</Heading>
    {children}
    <Text>Women of all abilities welcome</Text>
    <Link to="https://www.strava.com/clubs/187536">Gazelles Strava</Link>
  </BorderBox>
)

const Roosters = () => (
  <BorderBox>
    <Heading size={runHeadingSize}>Roosters Run</Heading>
    <Text>Roosters Run 6am at Nolan St Carpark, Lake Weeroona</Text>
    <Text>Distances: 4-6km Loop All levels catered for.</Text>
  </BorderBox>
)

const GavsIntervals = () => (
  <BorderBox>
    <Heading size={runHeadingSize}>Gavin Fielder’s Interval Sessions </Heading>
    <Text>
      6PM on the railway side of the Rowing Club Car Park, Lake Weeroona
    </Text>
    <Text>Various and diverse intervals. All Ages, all Abilities</Text>
  </BorderBox>
)

// eslint-disable-next-line react/display-name
export default () => (
  <Layout title="Regular Weekly Runs">
    <Text>
      There are few weekly events that are more or less connected to the club
    </Text>
    <Heading size={dayHeadingSize}>Tuesday</Heading>
    <Gazelles>
      <Text>Tuesdays at the Track at 6am</Text>
    </Gazelles>
    <Roosters />
    <Heading size={dayHeadingSize}>Wednesday</Heading>
    <GavsIntervals />
    <Heading size={dayHeadingSize}>Thurday</Heading>
    <Gazelles>
      <Text>Lake Weeroona Northen Carpark at 6am</Text>
    </Gazelles>
    <Roosters />
    <Heading size={dayHeadingSize}>Saturday</Heading>
    <BorderBox>
      <Text>
        <Link to="/about/summer-track">Summer Track</Link> in the summer,{' '}
        <Link to="/runs">Club Runs</Link> in the Cooler runs
      </Text>
    </BorderBox>
    <BorderBox>
      <Heading size={runHeadingSize}>ParkRun</Heading>
      <Text>
        Bendigo is richer for 2 park runs{' '}
        <Link to="https://www.parkrun.com.au/bendigobotanicgardens/">
          Bendigo Botanic Gardens
        </Link>{' '}
        and{' '}
        <Link to="https://www.parkrun.com.au/kenningtonreservoir/">
          Kennington Reservoir
        </Link>
      </Text>
    </BorderBox>
    <Heading size={dayHeadingSize}>Sunday</Heading>
    <BorderBox>
      <Heading size={runHeadingSize}>The Buchanan Run</Heading>
      <Text>7am SHARP</Text>
      <Text>
        John Bomford Centre Carpark at Kennington Reservoir (Cnr Condon st &amp;
        Crook St)
      </Text>
      <Text>
        15km - 30km of easy jogging through hills and / or flat. All welcome.
      </Text>
      <Text>
        There pack tends to self seed and head off at various paces, and various
        distances.
      </Text>
    </BorderBox>
  </Layout>
)
