import React from 'react'
import Layout from 'components/Layout'
import {
  Stat,
  StatGroup,
  StatHelpText,
  StatLabel,
  StatNumber,
  Text,
  useColorModeValue,
} from '@chakra-ui/react'
import { avURL, Link } from 'components/Link'
import { Heading } from 'components/Chakra'
import { MailTreasurer } from 'pages/about'

export default () => (
  <Layout title="Fee Structure" joinOnline>
    <Text>
      At the 2020 AGM, a new fee structure was endorsed. It aims to offer a
      broader selection of ways to financially support the club without
      increasing membership overall fees.
    </Text>
    <Heading size="md">Annual Subscription Fee</Heading>
    <Text>
      This is your Annual Membership fee, and is valid from post the AGM until
      the end of September the following year. All members, including AV (
      <Link to={avURL}>Ahtletics Victoria</Link>) registered, will be required
      to pay this.
    </Text>
    <Heading size="md">Winter Cross Country Race Fee</Heading>
    <Text>
      This has been introduced for Non-Athletics Victoria members who wish to
      participate in the Winter Cross Country season. AV members will have this
      fee waived.
    </Text>
    <Heading>Example Payments</Heading>
    <Text>
      Covering most example situations. Contact <MailTreasurer /> is you are
      unsure of your particular situation.
    </Text>
    <Heading size="md">Non AV Registered</Heading>
    <StatGroup bg={useColorModeValue('blue.200', 'blue.500')}>
      <Stat>
        <StatLabel>Student (No AV)</StatLabel>
        <StatNumber>$10</StatNumber>
        <StatHelpText>Annual</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Student (No AV)</StatLabel>
        <StatNumber>$10</StatNumber>
        <StatHelpText>Winter </StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Student (No AV)</StatLabel>
        <StatNumber>$20</StatNumber>
        <StatHelpText>Total</StatHelpText>
      </Stat>
    </StatGroup>
    <StatGroup bg={useColorModeValue('green.200', 'green.500')}>
      <Stat>
        <StatLabel>Open (No AV) </StatLabel>
        <StatNumber>$20</StatNumber>
        <StatHelpText>Annual</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Open (No AV)</StatLabel>
        <StatNumber>$30</StatNumber>
        <StatHelpText>Winter</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Open (No AV)</StatLabel>
        <StatNumber>$50</StatNumber>
        <StatHelpText>Total</StatHelpText>
      </Stat>
    </StatGroup>
    <StatGroup bg={useColorModeValue('orange.200', 'orange.500')}>
      <Stat>
        <StatLabel>Family (No AV)</StatLabel>
        <StatNumber>$40</StatNumber>
        <StatHelpText>Annual</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Family (No AV)</StatLabel>
        <StatNumber>$60</StatNumber>
        <StatHelpText>Winter</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Family (No AV)</StatLabel>
        <StatNumber>$100</StatNumber>
        <StatHelpText>Total</StatHelpText>
      </Stat>
    </StatGroup>
    <Heading size="md">AV Registered</Heading>
    <StatGroup bg={useColorModeValue('blue.200', 'blue.500')}>
      <Stat>
        <StatLabel>Student (AV)</StatLabel>
        <StatNumber>$10</StatNumber>
        <StatHelpText>Annual</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>$10 Waived</StatLabel>
        <StatNumber>$0</StatNumber>
        <StatHelpText>Winter </StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Student (AV)</StatLabel>
        <StatNumber>$10</StatNumber>
        <StatHelpText>Total</StatHelpText>
      </Stat>
    </StatGroup>
    <StatGroup bg={useColorModeValue('green.200', 'green.500')}>
      <Stat>
        <StatLabel>Open (AV) </StatLabel>
        <StatNumber>$20</StatNumber>
        <StatHelpText>Annual</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Waived</StatLabel>
        <StatNumber>$0</StatNumber>
        <StatHelpText>Winter</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Open (AV)</StatLabel>
        <StatNumber>$20</StatNumber>
        <StatHelpText>Total</StatHelpText>
      </Stat>
    </StatGroup>
    <StatGroup bg={useColorModeValue('orange.200', 'orange.500')}>
      <Stat>
        <StatLabel>1 Adult AV</StatLabel>
        <StatNumber>$40</StatNumber>
        <StatHelpText>Annual</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>$30 Waived</StatLabel>
        <StatNumber>$30</StatNumber>
        <StatHelpText>Winter</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Family</StatLabel>
        <StatNumber>$70</StatNumber>
        <StatHelpText>Total</StatHelpText>
      </Stat>
    </StatGroup>
    <StatGroup bg={useColorModeValue('orange.200', 'orange.500')}>
      <Stat>
        <StatLabel>2 Adult AV</StatLabel>
        <StatNumber>$40</StatNumber>
        <StatHelpText>Annual</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>2x$30 Waived</StatLabel>
        <StatNumber>$0</StatNumber>
        <StatHelpText>Winter</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Family</StatLabel>
        <StatNumber>$40</StatNumber>
        <StatHelpText>Total</StatHelpText>
      </Stat>
    </StatGroup>
    <StatGroup bg={useColorModeValue('orange.200', 'orange.500')}>
      <Stat>
        <StatLabel>1 AV Adult 1 AV Child</StatLabel>
        <StatNumber>$40</StatNumber>
        <StatHelpText>Annual</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>1x$30 1x$10 Waived</StatLabel>
        <StatNumber>$20</StatNumber>
        <StatHelpText>Winter</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Family</StatLabel>
        <StatNumber>$60</StatNumber>
        <StatHelpText>Total</StatHelpText>
      </Stat>
    </StatGroup>
    <StatGroup bg={useColorModeValue('orange.200', 'orange.500')}>
      <Stat>
        <StatLabel>2 AV Child</StatLabel>
        <StatNumber>$40</StatNumber>
        <StatHelpText>Annual</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>2x$10 Waived</StatLabel>
        <StatNumber>$40</StatNumber>
        <StatHelpText>Winter</StatHelpText>
      </Stat>
      <Stat>
        <StatLabel>Family</StatLabel>
        <StatNumber>$80</StatNumber>
        <StatHelpText>Total</StatHelpText>
      </Stat>
    </StatGroup>
  </Layout>
)
