import React from 'react'
import Layout from 'components/Layout'
import {
  Box,
  Wrap,
  WrapItem,
  Spacer,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from '@chakra-ui/react'
import { Heading, Text } from 'components/Chakra'
import Carousel from 'nuka-carousel'

//import { LinkAthsBendigo, MailPresident, MailSummerCaptain } from 'pages/about'

// eslint-disable-next-line react/display-name

const wrapBoxWidth = 'xs'

export default () => (
  <Layout title="Official Club Merchandise">
    <Text>Get yours and look as cool as the rest of us</Text>
    <Text>
      These custom designed official club singlets and t-shirts are made in
      Australia from lightweight, moisture wicking polyester. The fully
      sublimated design looks great and is AV approved.
    </Text>
    <Wrap>
      <WrapItem>
        <Box width={wrapBoxWidth}>
          <Heading size="md">Run Singlets</Heading>
          <Text>
            Available in Mens, Womens and Kids razor back and Womens regular
            (wide back).
          </Text>

          <Box width="270px">
            <Carousel
              swiping
              autoplay
              renderCenterLeftControls={({ previousSlide }) => (
                <button onClick={previousSlide}>&#10094; </button>
              )}
              renderCenterRightControls={({ nextSlide }) => (
                <button onClick={nextSlide}> &#10095;</button>
              )}
            >
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/MensSingletfront.jpg" />
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/MensSingletback.jpg" />
            </Carousel>
          </Box>
        </Box>
      </WrapItem>
      <WrapItem>
        <Box width={wrapBoxWidth}>
          <Heading size="md">Run T-Shirt</Heading>
          <Text>Available in Mens, Womens and Kids.</Text>
          <Spacer />
          <Box maxW="270px">
            <Carousel
              swiping
              autoplay
              renderCenterLeftControls={({ previousSlide }) => (
                <button onClick={previousSlide}>&#10094; </button>
              )}
              renderCenterRightControls={({ nextSlide }) => (
                <button onClick={nextSlide}> &#10095;</button>
              )}
            >
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/MensT-Shirtfront.jpg" />
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/MensT-Shirtback.jpg" />
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/Long-t.jpg" />
            </Carousel>
          </Box>
        </Box>
      </WrapItem>
      <WrapItem>
        <Box width={wrapBoxWidth}>
          <Heading size="md">Crop Tops ON SALE</Heading>
          <Text>
            We have a limited number of XS and S Women’s crop tops. Once sold we
            will not be restocking.
          </Text>
          <Spacer />
          <Box maxW="270px">
            <Carousel
              swiping
              autoplay
              renderCenterLeftControls={({ previousSlide }) => (
                <button onClick={previousSlide}>&#10094; </button>
              )}
              renderCenterRightControls={({ nextSlide }) => (
                <button onClick={nextSlide}> &#10095;</button>
              )}
            >
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/Crop-front.jpg" />
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/Crop-back.jpg" />
            </Carousel>
          </Box>
        </Box>
      </WrapItem>
      <WrapItem>
        <Box width={wrapBoxWidth}>
          <Heading size="md">Hoodies</Heading>
          <Text>Available in grey and black</Text>
          <Text>
            Keep warm on those cold Saturdays before your run or wear it proud
            to your next interclub or long distance event. The Bendigo Uni hoody
            is stylish and remarkably good value for money.
          </Text>
          <Text>
            Made from 370gsm 80% cotton 20% poly, this super heavy fleece
            features a kangaroo pocket with rib banding and a fully lined hood
            with 'natural' cord.
          </Text>
          <Spacer />
          <Box maxW="270px">
            <Carousel
              swiping
              autoplay
              renderCenterLeftControls={({ previousSlide }) => (
                <button onClick={previousSlide}>&#10094; </button>
              )}
              renderCenterRightControls={({ nextSlide }) => (
                <button onClick={nextSlide}> &#10095;</button>
              )}
            >
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/Club-Hoody.jpg" />
            </Carousel>
          </Box>
        </Box>
      </WrapItem>
      <WrapItem>
        <Box width={wrapBoxWidth}>
          <Heading size="md">Beanies and Buffs</Heading>
          <Text>
            Keep your head warm on those cold Saturdays before your run
          </Text>
          <Box maxW="270px">
            <Carousel
              swiping
              autoplay
              renderCenterLeftControls={({ previousSlide }) => (
                <button onClick={previousSlide}>&#10094; </button>
              )}
              renderCenterRightControls={({ nextSlide }) => (
                <button onClick={nextSlide}> &#10095;</button>
              )}
            >
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/BUAC-Beanie-0.png" />
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/BUAC-Beanie-1.png" />
              <img src="https://bendigouniathsclub.org.au/assets/images/uniform/BUAC-Buff.jpg" />
            </Carousel>
          </Box>
        </Box>
      </WrapItem>

      <WrapItem>
        <Box width={wrapBoxWidth}>
          <Heading size="md">Availability</Heading>
          <Text>No online store! Uniforms are available at club events.</Text>
        </Box>
      </WrapItem>
      <WrapItem>
        <Box>
          <Heading size="md">Pricing</Heading>
          <Text>
            These very high quality items are very close to cost price
          </Text>
          <Table>
            <Thead>
              <Tr>
                <Th>Item</Th>
                <Th>Brand</Th>
                <Th>Price</Th>
              </Tr>
            </Thead>
            <Tbody>
              <Tr>
                <Td>BUAC Beanie</Td>
                <Td></Td>
                <Td>$25</Td>
              </Tr>
              <Tr>
                <Td>BUAC Buff</Td>
                <Td></Td>
                <Td>$25</Td>
              </Tr>
              <Tr>
                <Td>Run Bendigo T-Shirt</Td>
                <Td></Td>
                <Td>$25</Td>
              </Tr>
              <Tr>
                <Td>Technical Singlet</Td>
                <Td></Td>
                <Td>$30</Td>
              </Tr>
              <Tr>
                <Td>Technical Short Sleeve T-Shirt</Td>
                <Td></Td>
                <Td>$35</Td>
              </Tr>
              <Tr>
                <Td>
                  Technical Long Sleeve T-Shirt
                  <hr />
                  Remaining stock half price. We will not be restocking.
                </Td>
                <Td>Catfish</Td>
                <Td>$20</Td>
              </Tr>
              <Tr>
                <Td>
                  Crop Top
                  <hr />
                  Once sold we will not be restocking.
                </Td>
                <Td>Catfish</Td>
                <Td>$20</Td>
              </Tr>
              <Tr>
                <Td>Trucker Cap</Td>
                <Td>Wattbomb</Td>
                <Td>$5</Td>
              </Tr>
            </Tbody>
          </Table>
          <Spacer />
        </Box>
      </WrapItem>
    </Wrap>
  </Layout>
)
