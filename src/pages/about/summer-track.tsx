import React from 'react'
import Layout from 'components/Layout'
import { Heading, Text } from 'components/Chakra'
import { LinkAthsBendigo, MailPresident, MailSummerCaptain } from 'pages/about'
import { SummerAthsBendigo } from 'pages/runs'

// eslint-disable-next-line react/display-name
export default () => (
  <Layout joinOnline title="Summer Track">
    <Text>
      Contact <MailPresident /> or the <MailSummerCaptain /> for further
      information on the BUAC track and field team, and what happens down the
      track over summer.
    </Text>
    <Text>
      They will probably explain that the club competes in various{' '}
      <LinkAthsBendigo /> events over the warmer months of the year
    </Text>
    <SummerAthsBendigo />

    <Heading>AV Shield</Heading>
    <Text>
      The AV Shield is a full program of track and field athletics on Saturday
      Afternoons.
    </Text>
    <Text>
      The Club is an Athletics Victoria affiliated club and has a track and
      field team that competes at every track meet.
    </Text>
    <Text>
      Uniform for the AV Shield is usually availble at the Track on Saturdays.
    </Text>
    <Text>
      The Club has end of season awards for track and field athletes in the
      following categories - throws, jumps, sprints and middle-long distance.
      Based on a points system.
    </Text>
  </Layout>
)
