import React from 'react'
import Layout from 'components/Layout'
import {
  Text,
  Box,
  // Flex,
  Stack,
  Button,
  Wrap,
  WrapItem,
  useBreakpointValue,
} from '@chakra-ui/react'

import { BorderBox, Heading } from 'components/Chakra'
import { Link } from 'components/Link'

import { useRouteData } from 'react-static'
import { RunMarkdown } from 'types/index'
import { DateTime } from 'luxon'
import {
  csvDateTime,
  mdRaceColor,
  raceDateTime,
  SummerAthsBendigo,
} from 'pages/runs'
import { LinkAthsBendigo, LinkAthsVic } from './about'
import { RunTypes } from 'lib/enums'
import { BuacCSV } from 'lib/race-folder'
import CSVChooser from 'components/CSVChooser'

type Data = {
  currentRaces: RunMarkdown[]
  recentResults: BuacCSV[][][]
  latestHandicapGroup: BuacCSV[][]
}

export default (): JSX.Element => {
  const { currentRaces, recentResults, latestHandicapGroup }: Data =
    useRouteData()
  // const now = DateTime.local().plus({ hours: 2 })
  const now = DateTime.local()
  const upcomingEnd = now.plus({ days: 29 })
  const comingUp = currentRaces
    .filter((r) =>
      [
        RunTypes.club.toString(),
        RunTypes.ab.toString(),
        // RunTypes.other.toString(),
      ].includes(r.markdown.type)
    )
    .filter((r) => raceDateTime(r) > now.minus({ hours: 2 }))
    .filter((r) => raceDateTime(r) < upcomingEnd)
  //.slice(0, 6)
  const resultsAfter = now.minus({ days: 29 })
  const shortRecentResults = recentResults.filter(
    (r) => csvDateTime(r[0][0]) > resultsAfter
  )
  const handicapsAfter = now.minus({ days: 10 })
  const recentHandicaps = latestHandicapGroup.filter(
    (r) => csvDateTime(r[0]) > handicapsAfter
  )
  const headSize = 'md'
  const comingCondition = comingUp.length > 0
  const resultsCondition = shortRecentResults.length > 0
  const handicapCondition = recentHandicaps.length > 0
  const harrierWarning = comingUp.filter(
    (r) => r.slug === '/runs/harriers-invite'
  )
  return (
    <Layout title="Bendigo University Athletics Club">
      <Box>
        {(comingCondition || resultsCondition || handicapCondition) && (
          <Wrap
            spacing={useBreakpointValue([0, 0, 0, 10])}
            alignContent="stretch"
            justifyContent="space-between"
          >
            {harrierWarning.length > 0 ? (
              <WrapItem>
                <BorderBox>
                  <Box>
                    <Heading size={headSize}>
                      Harriers Invite Location Warning
                    </Heading>
                    <Text>
                      Please note that the Harriers Invite location is now at
                      the Pony Club in South Mandurang
                    </Text>
                    <Text>
                      Depending on when it was printed, The calander on your
                      fridge might specify have the location as Quarry Hill.
                    </Text>
                    <Text>Quarry Hill location is incorrect.</Text>
                  </Box>
                </BorderBox>
              </WrapItem>
            ) : null}
            {comingCondition && (
              <WrapItem>
                <BorderBox>
                  <Box>
                    <Link to="/runs">
                      <Heading size={headSize}>Upcoming Events</Heading>
                    </Link>
                  </Box>
                  <Wrap spacing="3">
                    {comingUp.map((r) => {
                      const { days, hours, minutes } = raceDateTime(r)
                        .diff(now, ['days', 'hours', 'minutes', 'seconds'])
                        .toObject()
                      const inText = `${days} day${
                        days == 1 ? '' : 's'
                      } ${hours}h:${minutes}m`
                      return (
                        <WrapItem key={r.slug}>
                          <Box>
                            <Text>{inText}</Text>
                            <Link to={r.slug}>
                              <Button bgColor={mdRaceColor(r)}>
                                {r.markdown.title}
                              </Button>
                            </Link>
                            <Text>
                              {raceDateTime(r).toFormat('EEE MMM dd')}
                            </Text>
                          </Box>
                        </WrapItem>
                      )
                    })}
                  </Wrap>
                </BorderBox>
              </WrapItem>
            )}
            {resultsCondition && (
              <WrapItem>
                <BorderBox>
                  <Box>
                    <Link to="/results">
                      <Heading size="md">Recent Results</Heading>
                    </Link>
                  </Box>
                  <Wrap
                    spacing={3}
                    alignContent="stretch"
                    justifyContent="space-between"
                  >
                    {shortRecentResults.map((race) => {
                      //const days = Math.ceil(race[0][0].raceFolder.frontMatter.date)
                      const csv = race[0][0]
                      return (
                        <WrapItem key={csv.slug}>
                          <Box>
                            <Text>{csv.raceFolder.frontMatter.name}</Text>
                            <Wrap>
                              {race.map((csvs) => (
                                <WrapItem key={csvs[0].slug}>
                                  <CSVChooser buttonSize="sm" csvs={csvs} />
                                </WrapItem>
                              ))}
                            </Wrap>
                          </Box>
                        </WrapItem>
                      )
                    })}
                  </Wrap>
                </BorderBox>
              </WrapItem>
            )}
            {handicapCondition && (
              <WrapItem>
                <BorderBox>
                  <Link to="/handicaps">
                    <Heading size="md">Handicaps </Heading>
                  </Link>
                  <Text>
                    {latestHandicapGroup[0][0].raceFolder.frontMatter.name}
                  </Text>
                  <Wrap>
                    {latestHandicapGroup.map((csvs) => (
                      <WrapItem key={csvs[0].slug}>
                        <CSVChooser buttonSize="sm" csvs={csvs} />
                      </WrapItem>
                    ))}
                  </Wrap>
                </BorderBox>
              </WrapItem>
            )}
          </Wrap>
        )}
      </Box>
      <BorderBox>
        <Stack spacing="3">
          <Text>
            The Bendigo University Athletics Club is a family friendly running
            club. Members span a wide range of ages, abilities and commitment.
            There is much more information{' '}
            <Link to="/about">about the club here</Link>, including{' '}
            <Link to="/about/uniform">club uniforms</Link>
          </Text>
          <SummerAthsBendigo />
          <Text>
            The main focus of the clubs organisational capabilities is the
            Winter Cross Country season. Each Saturday at 2pm, between 100 and
            200 people gather in <Link to="/runs">various bush settings</Link>{' '}
            in Bendigo area to run and have afternoon tea
          </Text>
          <Text>
            The club is affiliated with <LinkAthsBendigo /> and <LinkAthsVic />{' '}
            which is a pathway for humble people from Bendigo who might take
            some of the steps required to go some of the way to the Olympics.
          </Text>
          <Text>
            As well as the <Link to="/runs">Club Runs</Link>, many members
            compete in the wider athletic community competitions organised by{' '}
            <LinkAthsBendigo /> and <LinkAthsVic /> and many others, even if
            they realise the Olympics might be a little out of reach.
          </Text>
          <Text>Many of us concentrate on the Afternoon Tea.</Text>
        </Stack>
      </BorderBox>
    </Layout>
  )
}
