import React, { useState } from 'react'
import { useRouteData } from 'react-static'
import { Link } from 'components/Link'
import { RunMarkdown } from 'types/index'
import Layout from 'components/Layout'
import {
  Box,
  Text,
  HStack,
  DarkMode,
  Checkbox,
  useBreakpointValue,
} from '@chakra-ui/react'
import { RunTypes } from 'lib/enums'
import { raceColors, prettyRaceType, mdRaceColor } from 'pages/runs'

type Data = {
  restingRaces: RunMarkdown[]
}

export default () => {
  const { restingRaces }: Data = useRouteData()
  const [raceTypeFilter, setRaceTypeFilter] = useState({
    [RunTypes.club]: true,
    [RunTypes.ab]: false,
    [RunTypes.av]: false,
    [RunTypes.other.toString()]: false,
  })

  const handleRaceTypeFilter = (e: React.ChangeEvent<HTMLInputElement>) =>
    setRaceTypeFilter({ ...raceTypeFilter, [e.target.name]: e.target.checked })

  const restingFiltered = restingRaces.filter(
    (r) => raceTypeFilter[r.markdown.type]
  )

  return (
    <Layout title="Resting Races">
      <Text>
        Schedule of races, organised by week, colour coded by organiser. There
        are also informal{' '}
        <Link to="/about/regular-runs">regular weekly runs</Link>
      </Text>
      <HStack p={2}>
        {Object.values(RunTypes).map((rt: string) => (
          <DarkMode key={rt}>
            <Checkbox
              key={rt}
              size={useBreakpointValue(['sm', 'md'])}
              color={raceColors(rt)}
              name={rt}
              isChecked={raceTypeFilter[rt]}
              onChange={handleRaceTypeFilter}
              // isDisabled={rt === RunTypes.club}
            >
              {prettyRaceType(rt)}
            </Checkbox>
          </DarkMode>
        ))}
      </HStack>
      <Box>
        <Text>
          Not all races can run every year. Some are a little dangerous now with
          100km/hr roads, some have not enough car parking...
        </Text>
        {restingFiltered.map((resting: RunMarkdown) => (
          <Box key={resting.slug}>
            <Link color={mdRaceColor(resting)} to={resting.slug}>
              {resting.markdown.title}
            </Link>
          </Box>
        ))}
      </Box>
    </Layout>
  )
}
