import React from 'react'
import { Root, Routes } from 'react-static'
import { Switch, Route } from 'react-router-dom'
import { ChakraProvider, ColorModeScript } from '@chakra-ui/react'
import theme from './theme/'

function App() {
  return (
    <>
      <ChakraProvider theme={theme}>
        <ColorModeScript initialColorMode={theme.config.initialColorMode} />
        <Root>
          <React.Suspense fallback={<em>Loading...</em>}>
            <Switch>
              <Route render={() => <Routes />} />
            </Switch>
          </React.Suspense>
        </Root>
      </ChakraProvider>
    </>
  )
}

export default App
