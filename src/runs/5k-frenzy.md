---
isCurrent: false
date: Feb 11 2022 6pm
title: The 5k Frenzy
location:
  text: Bendigo Regional Athletics Complex, Cnr Cook St & Retreat Rd
  href: https://www.google.com.au/maps/dir//Athletics+Track,+La+Trobe+University,+58+Retreat+Rd,+Flora+Hill+VIC+3550/@-36.7843614,144.2870447,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x6ad75a24dae1667b:0x1f200f28d8eaf541!2m2!1d144.2891802!2d-36.7843577!3e0
setter:
  href: https://www.bendigoharriers.org/
  text: Bendigo Harriers
distances: 5k
type: AB
---

An initiative of the Bendigo Harriers Athletics Club, being run alongside the
City of Greater Bendigo’s “Summer in the Parks” Festival is the inaugural 5km
Frenzy event.

Live bands, circus performers, roaming mascots and food outlets, it is a
fantastic carnival atmosphere. 

Spectators are be able get up close and on track to watch some of the best
5000m runners in Victoria.

The Atmosphere generated is terrifically exciting for runners and spectators
alike.  

[Youtube live stream](https://www.youtube.com/watch?v=GOPUs8HcNG4)
