---
isCurrent: false
title: Victorian Schools XCR Relays
date: May 21 2022
type: AV
location:
  text: Jells Park
  href: https://athsvic.org.au/avevents/
setter:
  - text: Athletics Victoria
    href: https://athsvic.org.au/
distances:
  - check AV
---

Jells Park will play host to the All Schools Cross Country Relays. Athletes
will start the course on a downhill progression where they will traverse
through trail/ bushland and undulating terrain before heading back uphill
towards the top of the course and to the start/finish. The course provides
great cross country running with excellent vantage points for schools and
spectators to support teams.
