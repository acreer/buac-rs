---
isCurrent: true
date: 2pm 2023/07/15
title: Harriers AC Invitation
layout: single
type: AB
location:
  text: South Mandurang Pony Club
  href: https://www.google.com.au/maps/dir//-36.8459323,144.2759268/@-36.8468424,144.2694251,16z
distances: 1km, 4km, and 8km
setter:
  text: Harriers AC
  href: https://www.bendigoharriers.org/
---

See the Harriers Website or Facebook for details.

For a few years there, BUAC awarded points and set handicaps for all the
invites. In those years files were created by the timing system.
