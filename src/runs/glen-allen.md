---
isCurrent: true
date: 2pm 2023/08/26
title: Glen Allen Memorial
type: AB
location:
  text: St Annes Windery Bendigo
  href: https://fb.me/e/38rdzCGpk
distances: 800m, 1500m, 3km, and 15km
setter:
  - text: South Bendigo AC
    href: https://www.southbendigoac.org.au/
  - text: Harriers AC
    href: https://www.bendigoharriers.org/
  - text: EagleHawk AC
    href: https://athsvic.org.au/club/eaglehawk-athletic-club/
---

The 2022 information is at [the book of faces](https://fb.me/e/38rdzCGpk)

In other years your mileage may vary.

There might be information in the [Atletic Bendigo Page](https://www.athleticsbendigo.org.au/?s=glen+allen)

There might also be information at [google](https://www.google.com/search?q=glen+allen+bendigo&oq=glen+allen+bendigo)
