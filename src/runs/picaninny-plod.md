---
title: Picaninny Plod
date: 2015-01-01 2pm
type: CLUB
isCurrent: false
location:
  text: Picaninny Rd
setter:
  text: Ben McDermid
distances:
  - the usual
---

This run is resting. There were too many blind hills, and a few cars going quite fast.
