---
isCurrent: false
date: Jan 18 2022, 7pm
title: Bendigo 5000m Championships
location: 
  text: The Track, 58 Retreat Rd Flora Hill
  href: https://www.google.com.au/maps/dir//Athletics+Track,+La+Trobe+University,+58+Retreat+Rd,+Flora+Hill+VIC+3550/@-36.7843614,144.2870447,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x6ad75a24dae1667b:0x1f200f28d8eaf541!2m2!1d144.2891802!2d-36.7843577!3e0
distances: 5k, 3k, 1k
setter: 
  text: Athletics Bendigo
  href: https://www.athleticsbendigo.org.au/
type: AB
---

One highlight from a series of [Atheletics Bendigo](https://www.athleticsbendigo.org.au/) Track races.

The Jack Davey 5000m is named after Bendigo Resident who ran track at
the British Empire Games and just missed out on representing Australia in
Melbourne at the 1956 games.

Article from the [Bendigo Advertiser](https://www.bendigoadvertiser.com.au/story/148728/jack-davey-recalls-olympic-crowds-roar/)

