---
isCurrent: false
date: Mar 15 2022, 7pm
title: Bendigo 10000m Championships
location: 
  text: The Track, 58 Retreat Rd Flora Hill
  href: https://www.google.com.au/maps/dir//Athletics+Track,+La+Trobe+University,+58+Retreat+Rd,+Flora+Hill+VIC+3550/@-36.7843614,144.2870447,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x6ad75a24dae1667b:0x1f200f28d8eaf541!2m2!1d144.2891802!2d-36.7843577!3e0
distances: 10k, 3k, 1k
setter: 
  text: Athletics Bendigo
  href: https://www.athleticsbendigo.org.au/
type: AB
---

One highlight from a series of [Atheletics Bendigo](https://www.athleticsbendigo.org.au/) Track races.

The Leigh Purtill 10000 metres Championship is named after a Bendigo Legend.

Only Athletics Victoria Registered athletes are eligible for the Championships. Both events are open to Invitaion athletes.
