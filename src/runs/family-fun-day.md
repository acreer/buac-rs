---
title: Family Fun Day
date: April 6 2019 11:00am
location:
  text: Latrobe University Athletics Track
  href: https://www.athleticsbendigo.org.au/the-complex-lubac
distances: Until 3pm
setter:
  text: The Committee
  href: https://www.bendigouniathsclub.org.au/ac/about.html#contacts
isCurrent: false
type: CLUB
---

Everyday day is a family fun day
