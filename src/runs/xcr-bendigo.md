---
title: XCR-3 St Anne’s Bendigo 8km
isCurrent: true
date: 2pm May 27 2023
type: AV
distances: 8km, 4km, 3km
location:
  text: St Annes Winery
  href: https://athsvic.org.au/events/xcr22-round-2-st-anne-bendigo-8km/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
---

What a way to kick-start the 2022 XCR individual rounds!

St Anne’s Bendigo offers sweeping landscapes of stunning views featuring
picturesque wetlands, ancient River Red Gums and vineyards. This is a runner’s
paradise. Why not make a weekend of it and bring the family, and take part in
the FAMILY3K walk/run.

See the BATS in action
