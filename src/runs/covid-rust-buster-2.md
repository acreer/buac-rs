---
isCurrent: false
date: April 10 2021 14:00
title: Handicap Reset Sedwick
type: CLUB
location:
  text: Sedwick Hall, Boyd Lane and Sedwick Rd
  href: https://www.google.com.au/maps/place/Sedwick+Hall/@-36.8715836,144.3135227,17z/data=!3m1!4b1!4m5!3m4!1s0x6ad745decb339169:0xcefd25545f65b69f!8m2!3d-36.8715836!4d144.3157114
distances: 1km, 3km, and 6.9km
setter:
  text: David Lonsdale
  href: https://www.strava.com/athletes/4125535
---

Very Similar to the Sedwick Scramble.

Come and refresh your handicap

Handicaps will be created for all members who run. Members must bring tag and
again nominate a pace.
