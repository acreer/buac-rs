---
title: XCR-2 Eastern Gardens
date: 2021-05-22 8am
type: AV
location:
  text: Eastern Park, Geelong
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
isCurrent: false
distances:
  - check AV
---

Eastern Park returns to the XCR Calendar after 11 years. It will be a tight and
fast course with small undulations, but be prepared for some possible strong
winds with the park being closely positioned near Geelong’s Eastern Beach.
Spectators will enjoy the course layout with fantastic viewing of the races as
athletes weave their way in and out of the trees.
