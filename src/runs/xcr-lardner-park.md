---
title: XCR-2 Lardner Park
date: 2021-05-22 2pm
type: AV
distances:
  - 8km
location:
  text: Lardner Park, Gippsland
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
isCurrent: false
---

Renowned as a challenging course, Lardner Park presents cross country running
at it’s best. Set amongst the rolling Gippsland pastures, this course
involves plenty of hills, ditches and fences to test cross country skills.
