---
isCurrent: true
date: 2pm 2023/08/19
title: Sedgwick Scramble
type: CLUB
location:
  text: Sedgwick Hall, Boyd Lane and Sedwick Rd
  href: https://www.google.com.au/maps/place/Sedgwick+Hall/@-36.8715836,144.3135227,17z/data=!3m1!4b1!4m5!3m4!1s0x6ad745decb339169:0xcefd25545f65b69f!8m2!3d-36.8715836!4d144.3157114
distances: 1km, 3km, and 6.9km
setter:
  text: David Lonsdale
  href: https://www.strava.com/athletes/4125535
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Sedgwick_Scramble_Long_8.05km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Sedgwick_Scramble_Medium_4.17km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Sedgwick_Scramble_Short_1.0km.html
    title: Short Couse Map
---

Good run in Sedwick

Nice Hill.
