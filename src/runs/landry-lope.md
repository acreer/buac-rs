---
isCurrent: true
date: 2pm 2023/07/22
title: Landry Lope
layout: single
type: CLUB
location:
  text: Lockwood Rd and Landry Track, Kangaroo Flat
  href: https://www.google.com.au/search?client=ubuntu&channel=fs&q=landry+track+and+lockwood+rd+kangaroo+flat&ie=utf-8&oe=utf-8&gfe_rd=cr&ei=9M3pWK38Eovr8wf_55jACQ
distances: 1km, 3km, and 6.9km
setter:
  text: Ben McDermid
  href: https://www.strava.com/athletes/434687
raceId: landry-lope
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Landry_Lope_Long_6.93km.html
    title: Long Couse Map
    padding: 220
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Landry_Lope_Medium_2.9km.html
    title: Medium Couse Map
    padding: 220
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Landry_Lope_Short_1.0km.html
    title: Short Couse Map
    padding: 220
---

Another cracker from Ben McDermid

Like all our runs consider the traffic on the approach roads.

Take High St to Kangaroo Flat.

Turn right into Lockwood Rd.

Drive approx 4.5km to Landry Track on Right – look for club signs.
