---
title: XCR-10 Tan Relays
isCurrent: true
date: Sep 16 2023
type: AV
location:
  text: The Tan Track, Melbourne Botanical Gardens
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
distances: 3.8km
---

Melbourne’s spiritual home of running, the Tan track in the heart of the
city’s sporting precinct. With a host of local and international
distance-running stars having made their mark on the hallowed course, this is
your chance to test yourself both against the greats and against the clock.
All competitors complete one lap of the 3.8km circuit, which winds its way
around the iconic Botanical Gardens and Kings Domain.
