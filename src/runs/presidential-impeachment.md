---
isCurrent: true
date: 2pm 2023/08/05
title: Presidential Impeachment
type: CLUB
location:
  text: Mount Sugarloaf Nature Conservation Reserve, Victoria
  href: https://www.google.com.au/maps/dir//36.70847%C2%B0+S,+144.47921%C2%B0+E/@-36.7238185,144.4422741,13z/data=!4m7!4m6!1m0!1m3!2m2!1d144.47921!2d-36.70847!3e0?entry=ttu
distances: L M S
setter:
  text: Ross Douglas
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Pres_Impeachment_Long_9.73km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Pres_Impeachment_Medium_3.47km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Pres_Impeachment_Short_1.0km.html
    title: Short Couse Map
---

Some people thought the drive in was a little rough.
