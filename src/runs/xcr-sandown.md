---
isCurrent: true
date: Jul 8 2023
title: XCR-5 Sandown Road Relays
type: AV
location:
  text: Sandown Racecourse
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
distances: 6.2k, 3.1km
---

Home to a number of motorsport events, Sandown Racecourse is the second relay
event for XCR’20. Held on the notoriously fast motor racing circuit, Sandown is
a venue where you can test your speed with either one or two laps of the 3.1km
circuit. So whether your testing the gears through the chicanes or going full
throttle on the main straight, Sandown will satisfy your need for speed.
