---
isCurrent: true
date: 2023/06/24
title: Rocky Reimagined
type: CLUB
distances: 1km 4km and 9km
location:
  text: Maiden Gully
  href: https://www.google.com.au/maps/dir//-36.7658388,144.2003308/@-36.7662063,144.2013808,16z/data=!4m2!4m1!3e0
setter:
  - text: Jayson Carter
    href: https://www.strava.com/athletes/4567493
iframes:
  - src: https://bendigouniathsclub.org.au/assets/darren-routes-copy/Rocky_Rises_Long_9.0km.html
    title: Long Couse Map
  - src: https://bendigouniathsclub.org.au/assets/darren-routes-copy/Rocky_Rises2_Medium_4.0km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Rocky_Rises_Short_1.0km.html
    title: Short Couse Map
---

Jayson Carter has re-imagined Rocky Rises long and medium courses.

The long is now a 9km loop, and the medium 4k has had a workover also.

From Olympic Parade Maiden Gully turn west on Rocky Rises Rd & travel for 2.5km

Nice fast run. Lots of muddy puddles on the approach if there has been rain
