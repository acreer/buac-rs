---
isCurrent: false
date: March 8 2022 6pm
title: Box Hill Burn
location:
  href: https://www.boxhillathleticclub.org/the-box-hill-classic/
  text: Boxhill Athletics Club
setter:
  href: https://www.boxhillathleticclub.org/the-box-hill-classic/
  text: Boxhill Athletics Club
distances: 5000m 1000m
type: OTHER
---

# A program of 5000m and 1000m graded races.

A great night of graded racing for old and young. Good place to set a PB in a race with people around your own pace.

