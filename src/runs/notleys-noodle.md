---
isCurrent: true
date: 2pm 2023/08/12
title: Notleys Noodle
layout: single
type: CLUB
location:
  text: Notley's Reserve, Whipstick
  href: https://www.google.com.au/maps/dir//Whipstick+campground/@-36.6553326,144.2657146,15.88z/data=!4m8!4m7!1m0!1m5!1m1!1s0x0:0xa07ed2959001b757!2m2!1d144.2632294!2d-36.6501666
distances: 1km, 4km, and 8km
setter:
  text: Ross Douglas
  href: https://www.strava.com/athletes/2532558
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Notleys_Noodle_Long_11.04km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Notleys_Noodle_Medium_4.74km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Notleys_Noodle_Short_1.0km.html
    title: Short Couse Map
---

Going round again because last year was so good.

People are not getting lost now.

Whipstick.

Notley’s Reserve, Whipstick: head north on Eaglehawk-Neilborough Road for approximately 10km. Turn left into Notley Road follow signs.
