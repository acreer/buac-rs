---
date: Apr 16 2022 10:30
title: The Dragon Mile
location:
  text: Pall Mall
  href: https://www.bendigoharriers.org/dragon-mile
setter:
  href: https://www.bendigoharriers.org/
  text: Bendigo Harriers
distances:
  - one mile
isCurrent: false
type: AB
---

The Bendigo Bank Dragon Mile has played a significant part in the athletics
history of Bendigo and plays a key part in the Easter Parade during this
festive period each year. It is run through the main streets of Bendigo
starting in Pall Mall. Crowds of approximately 20,000 line the streets to watch
the race.
