---
title: Diamond Hill Dash
date: 2015-01-01 2pm
type: CLUB
isCurrent: false
location:
  text: Lawson St and Fallaugh Ballaugh Rd
setter:
  text: Gary Crouch
distances:
  - the usual
---

This run is resting. Good long hill, but 100km/hr bitumen. Not much
carparking at the start.
