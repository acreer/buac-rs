---
isCurrent: true
date: 2pm 2023/09/02
title: Mystery Run
type: CLUB
location:
  text: its a mystery
  href: https://www.merriam-webster.com/dictionary/mystery
distances: Its a mystery....
setter:
  text: Also a Mystery
---

Its a mystery.........

BUT if its too much of a mystery noone will know where to go.

This run is new and secret. The idea

- Nominate your own speed
- Run without time or gps devices (no watches, hour glass, satelite receivers)
- Win by being accurate to your prediction

Also a way for new runs to become favorites.

## Please Note

These runs are all on different courses, so comparing results over the years is
not a fair comparison. Also the aim is to run at a set speed, not fast!

Although nominating you maximum speed might be a good tactic.
