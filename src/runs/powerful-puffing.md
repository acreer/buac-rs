---
isCurrent: true
date: 2pm 2023/06/17
title: Powerful Puffing
type: CLUB
location:
  text: Wildflower and Powerlines
distances: 1km, 3km, and 6km
setter:
  - text: Gavin Fielder
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Powerful_Puffing_Long_7.81km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Powerful_Puffing_Medium_3.73km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Powerful_Puffing_Short_1.0km.html
    title: Short Couse Map
---

Run combines Wildflower Drive and THE powerlines.
