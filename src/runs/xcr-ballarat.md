---
title: XCR-7 Ballarat Road Race
isCurrent: true
date: Aug 5 2023
type: AV
distances: 15k
location:
  text: Lake Wendouree
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
---

NOTE Date TBC. May be the Sunday Aug 6

The traditional stomping ground of XCR legend Steve Moneghetti, Lake Wendouree
once again provides the backdrop for this fast-paced event, with open athletes
completing 15km and juniors 6km courses. After the race, stay on and enjoy the
best the city and surrounding Goldfields have to offer, including the Eureka
Centre, Sovereign Hill, the Gold Museum and the Botanical Gardens.
