---
title: XCR-6 Bundoora State XC Championships
isCurrent: true
date: Jul 22 2023
type: AV
distances: 10k
location:
  text: Bundoora Park
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
---

Note: Selection Trial for the Australian Cross Country Championships.

This bumper day of cross country racing incorporates the Victorian All Schools
Cross Country Championships and also serves as the selection trial for the
Australian Cross Country Championships. Both the men and women races cover 10km
of the gruelling slopes of Bundoora Park, while juniors race over 3km, 4km, 6km
or 8km.
