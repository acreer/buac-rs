---
isCurrent: false
title: Mothers Day Classic
date: May 8 2022
type: OTHER
location:
  text: Bendigo
  href: https://www.mothersdayclassic.com.au/vic-bendigo
setter:
  text: Mothers Dat Classic
  href: https://www.mothersdayclassic.com.au/
distances:
  - check the website
---

# Walk or run for breast cancer research

Mothers Day Classic are encouraging Australians to get active for breast
cancer this Mother’s Day. You can set yourself a goal to work towards in the
lead up to the big day. You will be able to track your kilometres on the
portal, get involved in the official training period and share your
achievements within the Facebook Group.

- Bridgewater
  href: https://www.mothersdayclassic.com.au/vic-bridgewater
- Other Locations
  href: https://www.mothersdayclassic.com.au/local
