---
isCurrent: true
title: Okeefe Trail Marathon
date: May 6 2023 7:00
type: OTHER
location:
  text: Junortoun to Heathcote
  href: https://www.theokeefe.com.au/run/
setter:
  text: https://www.theokeefe.com.au/
  href: https://www.theokeefe.com.au/
distances: 5 10 21 42.2
---

Follow the spectacular O’Keefe Rail Trail from Bendigo to Heathcote, as it weaves its way through bushland, open fields, farms and past Lake Eppalock.

The O’Keefe Challenge is more than just a race, fun run or bike ride – it celebrates fitness, nature and the community spirit of the Heathcote and Bendigo region.
