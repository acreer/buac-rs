---
isCurrent: true
date: 2pm 2023/04/15
title: College Classic
type: CLUB
location:
  text: Kairn Rd Strathdale
  href: https://www.google.com.au/maps/dir//Kairn+Rd,+Strathdale+VIC+3550/@-36.7826574,144.3116159,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x6ad7597d4c8d1631:0x268d2890dbf0b1e!2m2!1d144.3138046!2d-36.7826574
distances: 1km, 3km, and 6km
setter:
  - text: Andrea Smith
    href: https://www.strava.com/athletes/12677228
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/College_Classic_Long_6.0km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/College_Classic_Medium_3.0km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/College_Classic_Short_1.0km.html
    title: Short Couse Map
---

Has seen a few start points, due mainly to people pressure.

Traditionally the first short flat run for the season.

No club runs are really short or flat
