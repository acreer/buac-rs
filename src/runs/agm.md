---
isCurrent: false
date: sep 17 2022
title: Annual General Meeting
location:
  text: To be confirmed
distances:
  - Not too long
setter:
  text: The Committee
  href: https://www.bendigouniathsclub.org.au/
type: CLUB
---

The AGM is your chance to get involved in the organisation of the club.

The pre/during meal option has been quite popular the past few years.

Whilst the main purpose is to elect the office bearers for the next 12 months you can also have a say in such weighty issues as:-

    - Which runs are good runs? Which not so good?
    - Suggest some new runs.
    - Order of club runs.
    - Runs in the school holidays?
    - How many runs do we need?
    - Do we need more 15k runs or more 5k runs?
    - When should the season start/end/break in the middle?

Or go the whole hog and organise a coup!

All welcome. See you there!
