---
isCurrent: false
date: Sep 25 2022
title: Australian Cross Country Champs
type: OTHER
distances: 10k
location:
  text: TBA
setter:
  text: Athetics Australia
  href: https://www.athletics.com.au/aa/events/
---

The peak event of Australian Cross Country running.

Has been won [twice](https://www.runnerstribe.com/latest-news/victorias-andrew-buchanan-and-madeline-hills-win-aussie-xc-championships/) by our very own Andy Buchanan
