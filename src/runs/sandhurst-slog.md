---
isCurrent: true
date: 2pm 2023/06/10
title: Sandhurst Slog
type: CLUB
location:
  text: Read Lane, Kangraroo Flat
  href: https://www.google.com.au/maps/dir/-36.8208001,144.2587206//@-36.8218736,144.25827,17z/data=!4m2!4m1!3e0
distances: 1km, 3km, and 7km
setter:
  - text: David Heislers
    href: https://www.strava.com/athletes/2613270
  - text: Matthew Heislers
    href: https://www.strava.com/athletes/2554855
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Sandhurst_Slog_Long_6.93km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Sandhurst_Slog_Medium_3.09km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Sandhurst_Slog_Short_1.0km.html
    title: Short Couse Map
---

This race was first run in 2012 as the end of year Mystery Run and has since been a regular fixture for yearly aggregate points.

There are 2 approach directions, depending on which part of Bendigo you are in

<h2>From Hattam St.</h2>

Golden Square travel south on Woodward Road then Diamond Hill
Road. Turn right into Kangaroo Gully Road and continue 500m to Read Lane on the
left.

<h2>From Kangaroo Flat</h2>

heading along Allingham Street (changes to Kangaroo
Gully Road), continue 2.5km south from intersection of Chapel Street to Read
Lane on the right.
