---
isCurrent: true
date: 2023-09-03 10:00
title: Run for Dad
layout: single
type: AB
location:
  text: Bendigo Racecourse Heinz Street White Hills.
  href: https://www.google.com.au/maps/dir//Bendigo+Jockey+Club,+Heinz+St,+White+Hills+VIC+3550/@-36.7278478,144.2817618,13z/data=!4m9!4m8!1m0!1m5!1m1!1s0x6ad758932b07b655:0x9b61814a66687929!2m2!1d144.316867!2d-36.727853!3e0
distances: 3.5km, 7.5km
setter:
  text: Athletics Bendigo
  href: https://www.athleticsbendigo.org.au/

header:
  overlay_image: https://runfordad.com.au/wp-content/uploads/2019/01/RunForDad_Cheer_1400.jpg
  overlay_color: blue
  overlay_filter: 0.5
  title: Run for Dad
  excerpt: Fathers Day September 3 2017
  cta_label: Enter Online
  cta_url: https://runfordad.com.au
---

Dress Up! Run with Dad

Start this Father's Day with the Moira Mac's "Run For Dad" fun run at the
Bendigo Racecourse.

Designed to raise your heart rate and much needed funds for Prostate Cancer
Foundation of Australia, we're calling on families from across Bendigo and
beyond to dress up as their favourite superhero to support the heroes fighting
prostate cancer.
