---
isCurrent: false
date: March 25 2022 6pm
title: Box Hill Classic
location:
  href: https://www.boxhillathleticclub.org/the-box-hill-classic/
  text: Boxhill Athletics Club
setter:
  href: https://www.boxhillathleticclub.org/the-box-hill-classic/
  text: Boxhill Athletics Club
distances: 3k 1500m  elite 800m
type: OTHER
---

# A program of 3000m and 1500m graded races.

A great night of graded racing for old and young. Good place to set a PB in a race with people around your own pace.

[2021 Youtube live stream](https://www.youtube.com/watch?v=H5qD6l6zvfo)
