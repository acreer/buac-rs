---
isCurrent: true
date: 3 September 2023
title: XCR-9 Victorian Half Marathon and 5K
type: AV
location:
  text: Burnley
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
distances: half marathon,5k
---

With its low gradient course along the banks of the Yarra River, the Burnley
Half Marathon has deservedly gained a reputation as one of the fastest half
marathons in Australia. With that in mind, the race is the perfect opportunity
to shoot for a personal best or tune up for the Melbourne Marathon.
