---
isCurrent: false
date: June 25 2022 14:00
title: Crusoe Crusade
layout: single
type: CLUB
location:
  text: Crusoe Number 7 Park
  href: https://www.google.com/maps/dir//-36.8322778,144.2318889/@-36.8322054,144.2296746,17z?hl=en
distances: 1km, 3km, and 8km
setter:
  text: TBA
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Crusoe_Crusade_Long_8.08km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Crusoe_Crusade_Medium_3.06km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Crusoe_Crusade_Short_1.0km.html
    title: Short Couse Map
---

Nice flat one today.
