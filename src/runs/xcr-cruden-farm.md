---
isCurrent: true
date: June 17 2023 12pm
title: XCR-4 TBC Cruden Farm
distances: 12k
type: AV
location:
  text: Cruden Farm
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
---

Cruden Farm will host the 12km heritage round. The grounds of Cruden Farm offer
athletes a tough and testing course with a mixture of different terrains to
accompany the beautiful grounds surrounding the farm. Cruden Farm is a historic
and inspirational venue and has been a great addition to the XCR calendar. The
2020 event is a great opportunity for Clubs to showcase their heritage and
history and we encourage Clubs to do so again this year.
