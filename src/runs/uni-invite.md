---
isCurrent: true
date: 2pm 2023/04/29
title: Bendigo University AC Invite
type: CLUB
location:
  text: Student Union, La Trobe University
  href: https://www.google.com/maps/dir//-36.7795056,144.3011185/@-36.7791035,144.3006702,303m/data=!3m1!1e3!4m2!4m1!3e0?hl=en
distances: 1km, 3km and 7.5km
setter:
  - text: Shayne Rushan
  - text: Gavin Fiedler
  - text: David Heislers
    href: https://www.strava.com/athletes/2613270
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/BUAC_Invitation_Long_7.6km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/BUAC_Invitation_Medium_3.05km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/BUAC_Invitation_Short_1.0km.html
    title: Short Couse Map
---

La Trobe University Bendigo Campus. Meet outside Student Union building.

A great run over some gentle undulations
