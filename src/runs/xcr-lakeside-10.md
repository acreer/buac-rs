---
isCurrent: true
title: XCR 2 Lakeside 10 Road Race
date: 8am May 7 2023
type: AV
location:
  text: Albert Park
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
distances: 10k
---

A fast and flat 10km road race, starting and finishing at the Lakeside Stadium
precinct which includes two laps out and back from Lakeside Stadium to Junction
Oval on Lakeside Drive. The Albert Park 10km Road Race is the perfect
opportunity for athletes to run a fast time or to just enjoy the surrounds of
the lake.
