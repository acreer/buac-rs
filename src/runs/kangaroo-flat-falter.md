---
isCurrent: false
date: jul 23 2022 14:00
title: Kangaroo Flat Falter
type: CLUB
location:
  text: Collins Street, Kangaroo Flat
  href: https://www.google.com/maps/dir//-36.7884578,144.2233338/@-36.7907057,144.231576,15.75z?hl=en-US
distances: 1km, 3km, and 8.2km
setter:
  - text: Justin Lee
    href: https://www.strava.com/athletes/4745036
  - text: Jenny Lee
  - text: Thomas Lee
  - text: Matthew Lee
images:
  - https://bendigouniathsclub.org.au/images/MainImages/KangarooFlat.jpg
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Kangaroo_Flat_Falter_Long_8.24km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Kangaroo_Flat_Falter_Medium_3.0km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Kangaroo_Flat_Falter_Short_1.0km.html
    title: Short Couse Map
---

Kangaroo Flat

Head South on High St to Kangaroo Flat. Turn right into Station St then right
into Olympic Pde. Take the 2nd left into Collins St and follow until dirt road.
Meet at the bridge.
