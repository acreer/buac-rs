---
isCurrent: false
date: 2023/06/24
title: Rocky Rises 10
type: CLUB
distances: 1km 4km and 10km
location:
  text: Maiden Gully
  href: https://www.google.com.au/maps/dir//-36.7658388,144.2003308/@-36.7662063,144.2013808,16z/data=!4m2!4m1!3e0
setter:
  - text: Possibly Kane Safstrom
    href: https://www.strava.com/athletes/48273567
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Rocky_Rises_Long_10.0km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Rocky_Rises_Medium_4.0km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Rocky_Rises_Short_1.0km.html
    title: Short Couse Map
---

From Olympic Parade Maiden Gully turn west on Rocky Rises Rd & travel for 2.5km

Nice fast run. Lots of muddy puddles on the approach if there has been rain
