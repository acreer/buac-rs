---
isCurrent: false
date: July 8 2017 14:00
title: AB Combined Mandurang
layout: single
type: AB
location:
  text: Nankervis Rd & Fadersons Ln, Mandurang
  href: https://www.google.com.au/maps/dir//-36.8270351,144.3037764/@-36.8269364,144.3048707,18z
distances: 1km, 4km, and 7.5km
setter:
  text: BUAC
  href: https://www.strava.com/clubs/119244
---

### Mandurang Cricket Ground

Travel South out Mandurang Rd, continue on to Faderson's Ln and over
Nankervis Rd. Turn right into cricket ground. See google map
