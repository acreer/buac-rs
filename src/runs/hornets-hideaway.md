---
isCurrent: true
date: 2pm 2023/07/08
title: Hornets Hideaway
setter:
  text: Craig Green
  href: https://www.strava.com/athletes/7537001
location:
  text: Crusoe No 7 Park
  href: https://www.google.com.au/maps/dir//-36.8315381,144.2330999/@-36.8247533,144.2263244,15z/data=!4m2!4m1!3e2
distances: 9.5km, 4.2km, 1km
type: CLUB
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Hornets_Hideaway_Long_9.51km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Hornets_Hideaway_Medium_4.18km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Hornets_Hideaway_Short_1.0km.html
    title: Short Couse Map
---

Run in the hills behind the Crusoe Res
