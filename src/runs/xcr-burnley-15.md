---
isCurrent: false
date: 5th September 2021 
title: XCR Burnley 5km and 15km XCR Round
distances: Junior 5km & Open/Masters 15km
type: AV
location:
  text: Burnley
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
---

Ballarat entries will be transferred to the Burnley 15k on Sunday, 5th September 2021. If you would like to seek a refund, don’t hesitate to get in touch with sportdelivery@athsvic.org.au.

