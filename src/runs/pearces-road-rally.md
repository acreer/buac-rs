---
isCurrent: true
date: 2pm 2023/06/03
title: Pearces Road Rally
layout: single
type: CLUB
location:
  text: Pearces Road, Mandurang
  href: https://www.google.com.au/maps/dir//-36.840644,144.3082778/@-36.8420373,144.2990109,16z/data=!4m2!4m1!3e2
distances: 1km, 3.6km, and 7.5km
setter:
  text: Ben McDermid
  href: https://www.strava.com/athletes/434687
images:
  - https://bendigouniathsclub.org.au/images/MainImages/PearcesRoad.jpg
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Pearces_Rd_Rally_Long_7.67km.html
    title: Long Couse Map
    padding: 122
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Pearces_Rd_Rally_Medium_3.67km.html
    title: Medium Couse Map
    padding: 122
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Pearces_Rd_Rally_Short_1.0km.html
    title: Short Couse Map
    padding: 122
---

No longer a mass starts for each race at the usual times.

Usual staggered start applies.

Pearces Road Rally sees a route of 7.6km rather than 7.5km from 2015 and
before. This 100m has an additional 3m of elevation gain with some lovely
single track replacing the Pearces Road section. Check out the new routes using
the links below.

Travel out Mandurang Rd, turn right into Nankervis Rd then left into Pearces Rd
meeting at intersection with Diggers Road.
