---
isCurrent: true
date: 2pm 2023/05/13
title: Mandurang Meander
type: CLUB
location:
  text: Nankervis Rd & Fadersons Ln, Mandurang
  href: https://www.google.com.au/maps/place/Nankervis+Rd+%26+Fadersons+Ln,+Mandurang+VIC+3551/@-36.8250934,144.3011977,17z/data=!3m1!4b1!4m5!3m4!1s0x6ad75b0284618fb9:0x13da83b4b48a6b60!8m2!3d-36.8250934!4d144.3033864
distances: 1km, 3km, and 6.5km
setter:
  - text: Andy Buchanan
    href: https://www.strava.com/athletes/3871363
  - text: Lee McCullagh
    href: https://www.strava.com/athletes/7851212
images:
  - https://bendigouniathsclub.org.au/images/MainImages/LeahCripps.jpg
  - https://bendigouniathsclub.org.au/images/MainImages/Mandurang.jpg
  - https://bendigouniathsclub.org.au/images/MainImages/MandurangGroupShot.jpg
  - https://bendigouniathsclub.org.au/images/MainImages/Manduranggirl.jpg
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Mandurang_Meander_Long_6.52km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Mandurang_Meander_Medium_3.0km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Mandurang_Meander_Short_1.0km.html
    title: Short Couse Map
---

This race was first run in 2009 as the end of year mystery run and has since
been a regular fixture for yearly aggregate points.

<hr>

Travel South out Mandurang Rd.

Continue on to Faderson's Lane and over Nankervis Rd.

Turn right into cricket ground.

The LONG course changed in 2013 from 6km to 6.5km
