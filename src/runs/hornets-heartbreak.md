---
isCurrent: false
date: Aug 20 2022 14:00
title: Hornets Heartache
setter:
  text: Craig Green
  href: https://www.strava.com/athletes/7537001
location:
  text: Crusoe No 7 Park
  href: https://www.google.com.au/maps/dir//-36.8315381,144.2330999/@-36.8247533,144.2263244,15z/data=!4m2!4m1!3e2
distances: 9.5km, 4.2km, 1km
type: CLUB
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Hornets_Heartache_Long_10.0km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Hornets_Heartache_Medium_4.35km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Hornets_Heartache_Short_1.0km.html
    title: Short Couse Map
---

Craig Green has massaged his Hornets Hideaway courses into hillier versions,
with the 10km Long Course going up to the ridgeline and back with some 190m of
gain (which is a smidgen more than the Sedgwick Scramble).

In the inaugural year, with so many trail events cancelled or
deferred this will be a sort of local, mini trail run.

Don't be deterred by the
climb there is a couple km of beautiful flowing downhill single trail to
recover on the way back.

Craig has posted some course maps on BUAC Facebook.

Run in the hills behind the Crusoe Res

This run has a similar start to the Hornets Hideaway, but much more climbing
