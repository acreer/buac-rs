---
isCurrent: true
title: Victorian Schools Road Relays
date: 2023/05/13
type: AV
location:
  text: Princes Park, Carlton North, VIC 3054
  href: https://athsvic.org.au/avevents
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
distances: various
---

Located at one of Melbourne’s most popular locations, this team-based relay
is around Princes Park in Carlton. Frequently used by elite sports club for
their time trials, the All Schools Road Relay Championships offer students an
exciting opportunity to compete with classmates in a high energy environment
in an inclusive and community-focused atmosphere.
