---
isCurrent: false
date: Apr 17 2021 14:00
title: Keith Huddle Memorial
layout: single
type: AB
location:
  text: Quarry Hill Recreation Reserve
  href: https://www.google.com.au/maps/dir//Ken+Wust+Oval,+51+Hamlet+St,+Quarry+Hill+VIC+3550/@-36.7744099,144.2745422,17z/
distances: 1km, 3km, and 6km
setter:
  text: Harriers AC
  href: https://www.bendigoharriers.org/
---

Quarry Hill
: Quarry Hill Recreation Reserve Ken Wust Oval Hamlet Street Quarry Hill.
