---
isCurrent: true
date: 2pm 2023-05-20
title: Ham Street Hustle
layout: single
type: CLUB
location:
  text: Ham Street Parking
  href: https://www.google.com.au/maps/dir//-36.7924414,144.2586475/@-36.7921624,144.2555939,17z
distances: 1km, 3km, and 6.9km
setter:
  - text: Andrew Creer
    href: https://www.strava.com/athletes/11003459
images:
  - https://bendigouniathsclub.org.au/assets/images/runs/ham-st/IMG_8024.jpg
  - https://bendigouniathsclub.org.au/assets/images/runs/ham-st/IMG_8025.jpg
iframes:
  - src: https://bendigouniathsclub.org.au/assets/darren-routes-copy/Ham_St_Hustle_Long_7.01km.html
    title: Long Course Map
    padding: 122.77
  - src: https://bendigouniathsclub.org.au/assets/darren-routes-copy/Ham_St_Hustle_Medium_3.19km.html
    title: Medium Course Map
    padding: 122.77
  - src: https://bendigouniathsclub.org.au/assets/darren-routes-copy/Ham_St_Hustle_Short_1.0km.html
    title: Short Course Map
    padding: 122.77
---

One of the Club Runs that everyone looks forward to for the sweet undulations.

The course setters have managed to make this run harder each year.

Some hate it but most LOVE it.

At least they love to complain about it.

From Hattam St. Golden Square.

Turn south into McDougal Rd.

Travel 1km then turn right into Ham Street.

Travel 500m and enter the old Unity Mining carpark on the left.
