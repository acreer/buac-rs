---
title: Half Marathon Festival
date: 2023-09-09 2pm
isCurrent: true
type: CLUB
location:
  text: Crusoe No 7
  href: https://www.bendigo.vic.gov.au/Things-to-Do/Natural-Reserves/Crusoe-Reservoir-and-No-7-Park
#  text: Golf Course Road, Epsom (2km East of Midland Hwy)
#  href: https://www.google.com.au/maps/dir///-36.7158379,144.3348403/@-36.7270754,144.3477068,14.5z/data=!4m2!4m1!3e2
distances: 1 km, 3km, 7km, 14km and 21.1km
setter:
  - text: David Lonsdale
    href: https://www.strava.com/athletes/4125535
iframes:
  - src: https://bendigouniathsclub.org.au/assets/race-maps/BUAC%20Half%20Marathon%20Crusoe/21km/BUAC_Half_Marathon_Crusoe_21km.html
    title: Half Crusoe
    padding: 150
  - src: https://bendigouniathsclub.org.au/assets/race-maps/BUAC%20Half%20Marathon%20Crusoe/14km/BUAC_Half_Marathon_Crusoe_14km.html
    title: Crusoe 14km
    padding: 150
  - src: https://bendigouniathsclub.org.au/assets/race-maps/BUAC%20Half%20Marathon%20Crusoe/7km/BUAC_Half_Marathon_Crusoe_7km.html
    title: Crusoe 7km
    padding: 150
---

Due to COVID uncertainty.

The venue for 2020, 2021 AND 2022 and 2023 is Crusoe Reservoir

Age/Distance categories

Ribbons will be awarded in each of the following distance gender and age
combinations.

Entry Fees

Online entry is encouraged, as it makes life easier for the organisers on the
day. It it cheaper as well

|     Entry Fee     | Online | On the day |
| :---------------: | :----: | :--------: |
|      Adults       |  \$10  |    \$15    |
| Children under 16 |  \$5   |    \$8     |
|     Students      |  \$5   |    \$8     |
|   BUAC members    |  FREE  |  As above  |

Distances are measured by hand with a wheel by old school enngineer types.
The distance is correct despite what your Garmin might say

| Distance |     Gender      |        Categories        |
| :------: | :-------------: | :----------------------: |
| 21.1 km  | Male and Female | Open, 40+, 50+, 60+, 70+ |
|  14 km   | Male and Female | Open, 40+, 50+, 60+, 70+ |
|   7 km   | Male and Female | Open, 40+, 50+, 60+, 70+ |
|   3 km   | Male and Female |      Open, U16, U12      |
|   1 km   | Male and Female |    Open, U10, U8, U6     |

World Famous in Bendigo and the greatest trail half in the Universe...

<h2>This map is for the traditional venue</h2>

The map of the Crusoe venue is available.
