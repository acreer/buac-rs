---
isCurrent: true
title: XCR-1 Cross Country Relays Jells Park
date: Apr 22 2023
type: AV
location:
  text: Jells Park
  href: https://athsvic.org.au/avevents/
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
distances: 6k
---

XCR Racing once again kicks off in the beautiful surrounds of Jells Park in
Melbourne’s south east with the Victorian Cross Country Relay Championships.
The challenging and undulating 3km course traverses both open grassland and
wooded forest areas, with open and overage competitors completing two laps per
leg and junior competitors one lap per leg.
