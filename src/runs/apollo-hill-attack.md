---
isCurrent: false
title: Apollo Hill Attack
date: 2015-01-01 2pm
type: CLUB
distances:
  - long
  - med
  - short
setter:
  text: Alan Buchanan
location:
  text: Apollo Hill
raceId: apollo-hill-attack
---

This run is resting. There is a section with blind corners on an increasingly
busy 100km/hr road.
