---
title: Piepers Plunder
date: July 7 2018 14:00
layout: single
type: CLUB
location:
  text: Wildflower Drive
  href: https://www.google.com.au/maps/dir//-36.7901966,144.3379397/@-36.7923213,144.3313317,15.5z/data=!4m2!4m1!3e2
distances: 1km, 4km, and 8km
setter:
  - text: David Lonsdale
    href: https://www.strava.com/athletes/4125535
  - text: Alan East
isCurrent: false
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Piepers_Plunder_Long_7.86km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Piepers_Plunder_Medium_3.93km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Piepers_Plunder_Short_1.0km.html
    title: Short Couse Map
---

2017 sees the return of a run last held in 2010.

### Strathfieldsaye

Heading out of Strathdale along Strathfieldsaye Road, Turn left into Wildflower Drive 1.5km after passing Guys Hill Road - look out for the club signs.
