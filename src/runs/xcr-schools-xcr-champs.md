---
isCurrent: true
title: Schools XC Relay
date: 2023/05/20
type: AV
location:
  text: Jells Park
setter:
  text: Athletics Victoria
  href: https://athsvic.org.au/
distances: various
---

The Victorian All Schools Cross Country Championships is the selection trial to
represent Victoria at the Australian Cross Country Championships.
