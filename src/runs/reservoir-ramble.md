---
date: May 26 2018 14:00
title: Reservoir Ramble
layout: single
type: CLUB
location:
  text: Sandhurst Reservoir Road
  href: https://www.google.com.au/maps/dir//-36.840465,144.2443422/@-36.8385828,144.2384581,15.25z
distances: 1km, 3km, and 7km
setter:
  text: David Lonsdale
  href: https://www.strava.com/athletes/4125535
isCurrent: false
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Reservoir_Ramble_Long_7.12km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Reservoir_Ramble_Medium_3.35km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Reservoir_Ramble_Short_1.0km.html
    title: Short Couse Map
---

2017 sees a new club run that was first introduced as the 2016 Mystery Run.

### Kangaroo Flat

Travel south on Calder Highway, turn left into Phillis Street, follow the bitumen over the railway line to the Sandhurst Reservoir gates
