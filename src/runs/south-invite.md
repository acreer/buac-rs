---
isCurrent: true
date: 2pm 2023/07/29
title: South Bendigo AC Invitation
type: AB
location:
  text: Woodvale Recreation Reserve
  href: https://www.google.com.au/maps/dir//-36.6839937,144.2275984/@-36.6895476,144.2198926,16z/data=!4m2!4m1!3e0
distances: 1km, 3.2km, and 6.4km
setter:
  - text: South Bendigo AC
    href: https://www.southbendigoac.org.au/
---

### Eaglehawk

Woodvale Recreation Reserve (Tennis Club), Janaway Rd, Woodvale.
