---
isCurrent: true
title: Rifle Range Rattle
date: 2pm 2023/04/22
layout: single
type: CLUB
location:
  text: Wellsford Rd and Popes Track, Junortoun
  href: https://www.google.com.au/maps/place/Wellsford+Rd+%26+Pope+Track,+Longlea+VIC+3551/@-36.7541957,144.3855404,17z/data=!4m5!3m4!1s0x6ad75f0942f64b31:0x77a68295d115e73a!8m2!3d-36.7571666!4d144.386248
distances: 1km, 3.4km, and 7.9km
setter:
  - text: Shayne Rushan
  - text: Frances Walsh
raceId: rifle-range-rattle
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Rifle_Range_Rattle_Long_7.92km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Rifle_Range_Rattle_Medium_3.44km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Rifle_Range_Rattle_Short_1.0km.html
    title: Short Couse Map
---

Aim High

Set your sights

<hr>

Travel East along McIvor Highway

turn left at Pope’s Road and follow the signs.
