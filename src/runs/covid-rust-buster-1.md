---
isCurrent: false
date: April 3 2021 14:00
title: Puddlers Plunder
type: CLUB
location:
  text: Crusoe Number 7 Park
  href: https://www.google.com/maps/dir//-36.8322778,144.2318889/@-36.8322054,144.2296746,17z?hl=en
distances: 1km, 3km, and 8km
setter:
  text: Ross Douglas
  href: https://www.strava.com/athletes/2532558
---

# The post covid first Rust Buster.

This is a Social run only, timing gear test only.

NO data re handicaps etc will be retained.

Members MUST bring tag and nominate a pace to be entered into the system for
a seeded start (mainly to avoid a mass start to observe social distancing
guidelines)

Long race is one lap of the Half Marathon Course for the long race.

Medium race very similar to the medium at the Crusoe Crusade.

Short race similar to short Crusoe Crusade.
