---
date: 2017-08-12 14:00
title: Trotting Terrace Trundle
layout: single
type: CLUB
location:
  text: Lords Raceway, Junortoun
  href: https://www.google.com.au/maps/dir//-36.7680375,144.3337085/@-36.7704788,144.3346924,16.71z/data=!4m2!4m1!3e0
distances: 1km, 3.4km, and 9.5km
setter:
  text: Tim Lauder
  href: https://www.strava.com/athletes/10739497
  text2: Harriers AC
  href2: https://www.bendigoharriers.org/
isCurrent: false
---

This race was held in combination with Harriers AC.

### Junortoun

Trotting Terrace (behind the trotting track).
