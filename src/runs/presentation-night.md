---
isCurrent: false
date: sep 17 2022
title: Presentation Day
type: CLUB
location:
  text: To be confirmed
distances:
  - Taxi is easiest
setter:
  text: The Committee
  href: https://www.bendigouniathsclub.org.au/ac/about.html#contacts
---

# Awards

Various awards are made based on results over the season.

Club Champions based on handcap results in the long, medium and short distances for both Male and Female

Speed Champions based on fastest times

# Entertainment

Entertaining reports from club personalities

Slide show of moments

# Events

Novelty races.
