---
date: June 30 2018 14:00
title: Junortoun Jog
layout: single
type: CLUB
location:
  text: Turners Road and Braeside Drive Junortoun
  href: https://www.google.com.au/search?as_q=Turners+Road+and+Braeside+Drive+Junortoun
distances: 1km, 3km, and 8.6km
setter:
  text: Greg McBain
isCurrent: false
iframes:
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Junortoun_Jog_Long_8.66km.html
    title: Long Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Junortoun_Jog_Medium_3.0km.html
    title: Medium Couse Map
  - src: https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/Junortoun_Jog_Short_1.0km.html
    title: Short Couse Map
---

### Junortoun

Heading out of Bendigo along Strathfieldsaye Road, turn left into Ryalls Lane.
Veer left into Junortoun Rd then turn left into Turners Rd and continue to the end.
Can also be accessed from McIvor Rd. See google map
