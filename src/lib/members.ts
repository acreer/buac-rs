import { readFileSync } from 'fs'
import Papa from 'papaparse'
import chalk from 'chalk'
import { nameLookup } from './member-name-lookup'

export type Club =
  | 'BUAC'
  | 'HARRIERS'
  | 'SOUTH BENDIGO'
  | 'EAGLEHAWK'
  | 'BYM'
  | 'OTHER'
  | 'GHY' // AV Glenhuntly

export type BuacMember = {
  id: string
  name?: string
  first?: string
  last?: string
  gender?: 'M' | 'F'
  tag?: string
  club: Club
}

const clubAlias = [
  { use: 'BUAC', aka: ['UNI', 'BAC', 'BEU'] },
  {
    use: 'HARRIERS',
    aka: ['HARRIERS', 'Bendigo Harriers', 'HAR', 'BEN', 'BGO'],
  },
  {
    use: 'SOUTH BENDIGO',
    aka: ['SBE', 'SOUTH BGO', 'STH BGO', 'SOUTH', 'STH', 'SBG', 'STH BENDIGO'],
  },
  { use: 'OTHER', aka: ['INV', 'INVITATION', 'NONE', 'GV'] },
  { use: 'BYM', aka: [] },
  { use: 'EAGLEHAWK', aka: ['EGWK', 'eYMCA', 'EGHWK', 'EWK'] },
  { use: 'BENDIGO YMCA', aka: ['BGO YMCA', 'BYMCA', 'BYM'] },
  { use: 'GHY', aka: [] },

  { use: 'ANW', aka: [] },
  { use: 'BEL', aka: [] },
  { use: 'BHA', aka: [] },
  { use: 'BOH', aka: [] },
  { use: 'BYC', aka: [] },
  { use: 'BYC', aka: [] },
  { use: 'CHI', aka: [] },
  { use: 'COL', aka: [] },
  { use: 'DKN', aka: [] },
  { use: 'EAG', aka: [] },
  { use: 'ESS', aka: [] },
  { use: 'FKN', aka: [] },
  { use: 'FKN', aka: [] },
  { use: 'GLG', aka: [] },
  { use: 'KSB', aka: [] },
  { use: 'MPA', aka: [] },
  { use: 'MUU', aka: [] },
  { use: 'SPA', aka: [] },
  { use: 'SSH', aka: [] },
  { use: 'UNA', aka: [] },
  { use: 'WEN', aka: [] },
  { use: 'WTN', aka: [] },
]

const consolidatedClub = (clubRaw: string): Club => {
  const club = clubRaw.replace(/\(|\)/g, '')
  const found = clubAlias.find(
    ({ use, aka }) =>
      use.toLowerCase() === club.toLocaleLowerCase() ||
      aka.find((a) => a.toLowerCase() === club.toLowerCase())
  )
  if (found) return found.use as Club
  console.log(chalk.red(`{use:${club},aka:[]} No club for ${club}`))
  return club as Club
}

export const consolidateName = (name: string): string => {
  const found = nameLookup.find(({ aka }) => aka.indexOf(name) > -1)
  if (found) return found.use
  return name
}

const unpaidRegex = /^\$/
export function cleanName(name: string): string {
  return (
    name
      .replace(unpaidRegex, '')
      // "Jackon   Eadon"
      .replace(/\s\s+/, ' ')
      .trim()
  )
}

function fixMember(fixed: BuacMember): BuacMember {
  if (!fixed.name && fixed.first && fixed.last) {
    fixed.name = cleanName(`${fixed.first} ${fixed.last}`)
  }
  if (!fixed.club) {
    fixed.club = 'OTHER'
  }
  fixed.name = cleanName(fixed.name)
  if (fixed.first) {
    fixed.first = cleanName(fixed.first)
  }
  return fixed
}

export class BuacMembers {
  loadedMembers: BuacMember[]

  static async loadMembers(filename: string): Promise<BuacMember[]> {
    const { data: members, errors } = Papa.parse(
      readFileSync(filename).toString(),
      {
        header: true,
      }
    )
    if (errors.length) {
      console.log(
        chalk.red(`Buac Members papaparse errors in ${filename}`),
        errors
      )
    }
    return members
      .filter(({ member }) => member)
      .map(({ first, last, sex, /* dob, */ member, tag }) => ({
        name: `${first} ${last}`,
        first,
        last,
        gender: sex,
        // dob: MembersDB.fixDOB(dob),
        id: member,
        tag,
        club: 'BUAC',
      }))
  }

  constructor(loadedMemebers: BuacMember[]) {
    this.loadedMembers = loadedMemebers
  }

  memberByName(name: string): BuacMember | null {
    const cleaned = consolidateName(cleanName(name))
    const matches = this.loadedMembers.filter(
      (member: BuacMember) =>
        member.name.toLocaleLowerCase() === cleaned.toLocaleLowerCase()
    )
    switch (matches.length) {
      case 1:
        return matches[0]
      case 0:
        return null
      default:
        throw new Error(
          `memberByName ${matches.length} matches for name:'${name}'`
        )
    }
  }

  memberById(id: string): BuacMember | null {
    const matches = this.loadedMembers.filter(
      (m: BuacMember) =>
        m.id.toString().toLowerCase() === id.toString().toLowerCase()
    )
    switch (matches.length) {
      case 1:
        return matches[0]
      case 0:
        return null
      default:
        throw new Error(
          `memberById '${matches.length}' members match id:'${id}'`
        )
    }
  }

  members(): BuacMember[] {
    return this.loadedMembers
  }

  addMember(member: BuacMember): BuacMember {
    const newMember = fixMember(member)
    if (newMember.club) {
      newMember.club = consolidatedClub(newMember.club)
    } else {
      newMember.club = 'OTHER'
    }
    this.loadedMembers.push(newMember)
    return newMember
  }

  memberName(member: BuacMember): string {
    return member.name ? member.name : `${member.first} ${member.last}`
  }
}
