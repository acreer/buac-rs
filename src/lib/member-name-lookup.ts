export const nameLookup = [
  {
    use: 'Andrew Buchanan',
    aka: ['Andy Buchanan'],
  },
  {
    use: 'Joshua Feuerherdt',
    aka: ['Josh Feuerherdt'],
  },
  {
    use: 'Mason Beesley',
    aka: ['Mason Woodward'],
  },
  {
    use: 'Ebony Beesley',
    aka: ['Ebony Woodward'],
  },
  {
    use: 'Bec Beesley',
    aka: ['Bec Woodward'],
  },
]
