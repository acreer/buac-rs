/* eslint-disable no-unused-vars */
export enum BuacTypes {
  result = 'RESULT',
  handicap = 'HANDICAP',
  speedPoints = 'SPEED_POINTS',
  clubPoints = 'CLUB_POINTS',
  dunno = 'DUNNO',
}

export enum RunTypes {
  club = 'CLUB',
  ab = 'AB',
  av = 'AV',
  other = 'OTHER',
}
