const chalk = require('chalk')
const jdown = require('jdown')
const chrono = require('chrono-node')
const { kebabCase } = require('lodash')
const { DateTime } = require('luxon')
const argv = require('yargs').option('ignore', {
  type: 'array',
  desc: 'dont list these types',
  alias: 'i',
}).argv

const runsDir = 'src/runs'
const typeColor = (t) =>
  t == 'CLUB'
    ? chalk.yellow
    : t == 'AV'
    ? chalk.blue
    : t == 'AB'
    ? chalk.green
    : t == 'OTHER'
    ? chalk.red
    : chalk.white

;(async () => {
  console.log(chalk.blue(`Getting race calander files`))
  console.log(`ob `, argv)
  console.log('ignore', argv.ignore)
  const allRace = Object.entries(await jdown(runsDir)).map(([key, value]) => {
    // console.log( `debug: key=${key} kebab=${kebabCase(key)} date=${value.date}`, value)
    if (value.date) {
      value.date = chrono.parseDate(value.date.toString())
    } else {
      console.log(value)
      throw new Error(`Somthing wrong with date '${value.date}' in ${key}`)
    }
    if (!value.raceId) {
      value.raceId = kebabCase(key)
    }
    return {
      slug: `/runs/${kebabCase(key)}`,
      markdown: value,
    }
  })
  const currentRaces = allRace
    .filter((r) => r.markdown.isCurrent)
    .sort((a, b) => a.markdown.date - b.markdown.date)
  const restingRaces = allRace
    .filter((r) => !r.markdown.isCurrent)
    .sort((b, a) => b.markdown.date - a.markdown.date)

  for (const r of currentRaces) {
    const colFn = typeColor(r.markdown.type) || chalk.grey
    console.log(
      DateTime.fromJSDate(r.markdown.date).toFormat(
        'MMM-dd EEE dd/MM/yyyy week-WW'
      ),
      '\t',
      colFn(r.markdown.type),
      '\t',
      colFn(r.markdown.title)
    )
  }
})()
