import chalk from 'chalk'
import { readdirSync } from 'fs'
import { partitionList } from '../utils'
import { kebabCase } from 'lodash'
import { stringify } from 'yaml'
;(async () => {
  const mapsDir = 'darren-routes/darren-routes-copy'
  console.log(chalk.blue(`reading maps from ${mapsDir}`))
  const issues: string[] = []
  const links = readdirSync(mapsDir)
    .map((l) => ({
      bits: l.match(/^(.*)_(Long|Medium|Short)_(.*)km.html$/),
      baseName: l,
      url: `https://www.bendigouniathsclub.org.au/assets/darren-routes-copy/${l}`,
    }))
    .filter(({ bits, baseName }) => {
      if (bits) return bits
      issues.push(chalk.red(`'${baseName} has no bits'`))
      return false
    })
    .map((v) => ({
      ...v,
      slug: kebabCase(v.bits[1]),
      title: `${v.bits[2]} Couse Map`,
    }))

  partitionList(links, ({ slug }: { slug: string }) => slug)
    .map(
      (slugGroup) => ({
        slug: slugGroup[0].slug,
        iframes: slugGroup.map(({ url: src, title }) => ({ src, title })),
      })
      // slugGroup.map(({ url: src, title }) => ({ src, title }))
    )
    .forEach((yaml) => console.log(stringify(yaml)))
  // console.log(g[0])

  // .forEach(( slugGrp:unknown[] )=>
  // 																						{
  // 																							console.log(stringify([ ({iframes: slugGrp.map({url:src,title})=>({src,title})) ] }))
  // 																						})
  // .forEach(({ url: src, title }) => console.log(stringify([{ src, title }])))

  // console.log(links)
  //issues.forEach((i) => console.log(i))
})()
