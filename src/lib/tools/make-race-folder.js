const chalk = require('chalk')
const jdown = require('jdown')
const chrono = require('chrono-node')
const { kebabCase, transform, camelCase } = require('lodash')
const { DateTime } = require('luxon')
const { join, basename } = require('path')
const {
  writeFileSync,
  mkdirSync,
  readFileSync,
  existsSync,
  unlinkSync,
  readdirSync,
  rmdirSync,
} = require('fs')
const YAML = require('yaml')

const runsDir = 'src/runs'

;(async () => {
  const allRace = Object.entries(await jdown(runsDir)).map(([key, value]) => {
    // console.log(`debug: key=${key} kebab=${kebabCase(key)}`)
    const dateString = value.date
    // console.log(dateString, typeof dateString)

    value.date = chrono.parseDate(dateString)
    if (!value.raceId) {
      value.raceId = kebabCase(key)
    }
    return {
      slug: `/runs/${kebabCase(key)}`,
      markdown: value,
    }
  })

  allRace.forEach((r) => {
    if (!r.markdown.type) {
      throw new Error(
        `No Type in ${r.markdown.title} ${JSON.stringify(
          r.markdown,
          null,
          ' '
        )}`
      )
    }
  })
  const clubRaces = allRace
    .filter((r) => r.markdown.type.toLowerCase() === 'club')
    .sort((a, b) => a.markdown.date - b.markdown.date)

  for (const race of clubRaces) {
    const date = DateTime.fromJSDate(race.markdown.date)

    const folder = join(
      'src',
      'data',
      'race-data',
      date.toFormat('yyyy'),
      basename(race.slug)
    )
    const fn = join(folder, 'frontMatter.yaml')

    if (race.markdown.isCurrent) {
      if (!existsSync(fn)) {
        console.log(
          chalk.blue(`Adding Race ${race.markdown.type} ${race.markdown.title}`)
        )
      }
      mkdirSync(folder, { recursive: true })
      writeFileSync(
        fn,
        YAML.stringify({
          date: date.toFormat('yyyy-MM-dd'),
          name: race.markdown.title,
          runMD: basename(race.slug),
        })
      )
    } else {
      if (existsSync(fn)) {
        console.log(chalk.blue(`Removing ${fn}`))
        unlinkSync(fn)
      }
      if (existsSync(folder) && readdirSync(folder).length == 0) {
        console.log(chalk.blue(`Remove empty folder ${folder}`))
        rmdirSync(folder)
      }
    }
  }
})()
