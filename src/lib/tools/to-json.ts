import chalk from 'chalk'
import {
  readFileSync,
  mkdirSync,
  writeFileSync,
  readdirSync,
  statSync,
} from 'fs'
import { join, dirname } from 'path'
import { flatten, startCase } from 'lodash'
import { BuacMember, BuacMembers } from '../members'
import { RaceFolder, BuacCSV } from '../race-folder'
import { BuacTypes } from '../enums'
import { resultsToPosts } from './club-posts'

import {
  partitionList,
  // buacCSVGenders,
  fastestRunner,
  removeRows,
} from '../utils'
import { sortBy } from 'lodash'

/**

Buac wants the
  - race data to have links to members,
  - members to be updated by people in races
  - maybe we should use a db or graphql
  - I am going to save stuff as json files.
**/

export interface WinnerRow {
  name?: string
  first?: string
  last?: string
  time?: string
  netTime?: string
  member?: string | number
}

export interface RaceWinner {
  csv: BuacCSV
  fastestFemale?: WinnerRow
  fastestMale?: WinnerRow
  clubWinner?: WinnerRow
}

const dataDir = './src/data/race-data'

// NOTE: json dir mentioned in tsconfig aslo
const jsonDir = './tmp-json'

export function walk(dir: string): string[] {
  let results: string[] = []
  readdirSync(dir).forEach((dirEntry: string) => {
    const walkPath: string = join(dir, dirEntry)
    const stat = statSync(walkPath)
    if (stat && stat.isDirectory()) {
      results = results.concat(walk(walkPath))
    } else {
      results.push(walkPath)
    }
  })
  return results
}

const saveObject = (obj: any, base: string) => {
  mkdirSync(dirname(base), { recursive: true })
  writeFileSync(join(jsonDir, `${base}.json`), JSON.stringify(obj, null, 2))
}

const liteCSVTypeByYearByEventByLength = (
  typeCSVs: BuacCSV[]
): BuacCSV[][][][] =>
  partitionList(
    typeCSVs.map((csv: BuacCSV) => removeRows(csv)),
    (csv: BuacCSV) => csv.raceFolder.year()
  ).map((byYear: BuacCSV[]) =>
    partitionList(byYear, (csv: BuacCSV) => csv.raceFolder.slug()).map(
      (byRace: BuacCSV[]) => {
        return partitionList(byRace, (csv: BuacCSV) => csv.buacLength)
      }
    )
  )

;(async () => {
  mkdirSync(jsonDir, { recursive: true })
  const memberDB = new BuacMembers(
    await BuacMembers.loadMembers(join(dataDir, 'member.csv'))
  )
  console.log(
    chalk.green(`Found ${memberDB.loadedMembers.length} members in the file`)
  )

  const allRaceFolder: RaceFolder[] = walk(dataDir)
    .filter((fn: string) => fn.match(/frontMatter\.yaml/))
    .map((fn: string) => new RaceFolder(fn))
    .sort(
      (a: RaceFolder, b: RaceFolder) =>
        b.frontMatter.date.valueOf() - a.frontMatter.date.valueOf()
    )
  // console.log(allRaceFolder[0])
  console.log(chalk.green(`loaded ${allRaceFolder.length} RaceFolders`))

  console.log(chalk.green('Loading Race csv files '))
  const allCSV = flatten(
    await Promise.all(
      allRaceFolder.map(
        async (raceFolder) => await raceFolder.loadCsvs(memberDB)
      )
    )
  ).sort((a, b) => {
    if (a.raceFolder.frontMatter.date === b.raceFolder.frontMatter.date) {
      return b.rows.length - a.rows.length
    }
    return (
      b.raceFolder.frontMatter.date.valueOf() -
      a.raceFolder.frontMatter.date.valueOf()
    )
  })
  saveObject(allCSV, 'allCSV')
  console.log(chalk.green(`Loaded ${allCSV.length} csv files`))
  console.log(
    chalk.green(`Now there are  ${memberDB.loadedMembers.length} members`)
  )

  saveObject(memberDB.loadedMembers, 'allMember')

  const allMember = memberDB.members()
  const allClub = partitionList(allMember, (m: BuacMember) => m.club).map(
    (members) => ({
      club: members[0].club,
      count: members.length,
    })
  )
  console.log(chalk.blue('Got clubs'), allClub)

  Object.values(BuacTypes).map((bType) => {
    const typeCSV = allCSV.filter((csv) => csv.buacType === bType)
    saveObject(
      liteCSVTypeByYearByEventByLength(typeCSV),
      `all${startCase(bType.replace('_', '').toLowerCase())}ByYearRaceLength`
    )
  })

  const allResultCSV: BuacCSV[] = allCSV.filter(
    (csv) => csv.buacType === BuacTypes.result
  )

  console.log(chalk.green('Making CSVLite no data races'))
  const allCSVLite = allCSV.map((csv) => removeRows(csv))
  saveObject(
    allRaceFolder.reduce((acc: any, rf: RaceFolder) => {
      acc[rf.folder] = Object.values(BuacTypes).reduce(
        (rfAcc: any, bType: string) => {
          rfAcc[bType] = partitionList(
            allCSVLite.filter(
              (c: BuacCSV) =>
                c.raceFolder.folder === rf.folder && c.buacType === bType
            ),
            (c: BuacCSV) => c.buacLength
          )
          return rfAcc
        },
        {}
      )
      return acc
    }, {}),
    'raceFolderTypeLengthGroups'
  )

  saveObject(
    partitionList(
      allResultCSV,
      (c: BuacCSV) => c.raceFolder.frontMatter.raceId
    ).map(
      (allForRaceId: BuacCSV[]) =>
        partitionList(allForRaceId, (c: BuacCSV) => c.raceFolder.year()).map(
          (yearGr: BuacCSV[]) =>
            partitionList(yearGr, (c: BuacCSV) => c.buacLength)
              .map((lenGr: BuacCSV[]) => {
                const allMatched = lenGr.find((c: BuacCSV) => c.fn.match(/ALL/))
                const csvToUse =
                  lenGr.length === 1 ? lenGr[0] : allMatched || lenGr[0] // The one with most results
                const hasClubWinner = lenGr.length === 0 // TODO && lenGr[0].raceFolder.frontMatter.
                return {
                  // Put 'debug' in and get rid of everything below to see what is happening
                  // debug: csvToUse.fn,
                  csv: removeRows(csvToUse),
                  fastestMale: fastestRunner(csvToUse, 'M', memberDB, false),
                  fastestFemale: fastestRunner(csvToUse, 'F', memberDB, false),
                  ...(hasClubWinner ? { clubWinner: csvToUse.rows[0] } : {}),
                }
              })
              .filter((i: RaceWinner) => i)
          // Turn the array into a lookup by length for easier processing at run time
          /*
              .reduce((acc: any, i: RaceWinner) => {
                if (i) acc[i.csv.buacLength] = i
                return acc
              }, {})
              */
        )
      // sometimes we cant decide which csv to use
      // .filter((g: any) => Object.keys(g).length > 0),
    ),
    'allRaceIdWinnersByYearByLength'
  )

  console.log(chalk.green('getting handicap and fastest times'))
  console.log(`there are ${allRaceFolder.length} race folders`)
  const allRaceWinnerByLength = allRaceFolder.map((raceFolder) => {
    // Race Folders are 'lite' and dont contain the csvs.
    // inefficient at build time is better.
    const allFolderCSV = allResultCSV.filter(
      (csv: BuacCSV) => csv.raceFolder === raceFolder
    )
    // return { folder: raceFolder.folder, csvs: allFolderCSV.length, }
    return (
      partitionList(allFolderCSV, (csv: BuacCSV) => csv.buacLength)
        // .filter((lengthCSVs) => lengthCSVs.length == 1) // TODO remove this line to proccess half mara invite etc.
        .map((lengthCSVs: BuacCSV[]) => ({
          folder: raceFolder.folder,
          length: lengthCSVs[0].buacLength,
          csvs: lengthCSVs.length,
          // csv: removeRows(lengthCSVs[0]),
          // Easy when there is only one file per length
          // clubWinner: lengthCSVs[0].rows[0],
          // fastestMale: fastestRunner(lengthCSVs[0], 'M', memberDB),
          // fastestFemale: fastestRunner(lengthCSVs[0], 'F', memberDB),
        }))
    )
  })
  // .filter((lengthList) => lengthList.length > 0) // remove future races with no results yet

  console.log(chalk.green('Trawling every run for every member'))
  saveObject(
    allMember.map((member) => ({
      member,
      csvs: partitionList(
        allResultCSV
          .filter((csv) => csv.rows.some((row) => row.member === member.id))
          .map((csv) => removeRows(csv)),
        (csv: BuacCSV) => csv.raceFolder.year()
      ).map((byYear: BuacCSV[]) =>
        partitionList(byYear, (csv: BuacCSV) => csv.raceFolder.folder).map(
          (byRace: BuacCSV[]) =>
            partitionList(byRace, (csv: BuacCSV) => csv.buacLength)
        )
      ),
    })),
    'allMemberResult'
  )

  // saveObject(allClub,  'allClub')
  saveObject(allRaceWinnerByLength, 'allRaceWinnerByLength')

  const allWpPost = flatten(
    walk(join(jsonDir, '/wp-cache/')).map((fn) =>
      JSON.parse(readFileSync(fn).toString())
    )
  )
  const allResultsGroups: BuacCSV[][][][] = require('../../../tmp-json/allResultByYearRaceLength.json')
  //const allResultsGroups: BuacCSV[][][][] = require('./tmp-json/allResultByYearRaceLength.json')
  const resultPost = resultsToPosts(allResultsGroups)
  console.log('post length', resultPost.length)
  const allPost = sortBy(
    resultPost.concat(allWpPost),
    (p: any) => p.date
  ).reverse()
  saveObject(allPost, 'allPost')
  console.log(chalk.green(`Got ${allPost.length} WP posts`))
})()
