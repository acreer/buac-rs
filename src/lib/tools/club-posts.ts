// import chalk from 'chalk'
import { BuacCSV } from '../race-folder'
import { DateTime } from 'luxon'
// import { writeFileSync } from 'fs'

export function resultsToPosts(allResultsGroups: BuacCSV[][][][]) {
  return allResultsGroups.flat(2).map((raceGroup: BuacCSV[]) => {
    //console.log(raceGroup)
    const year = raceGroup[0].raceFolder.year
    const { name, date: dateString } = raceGroup[0].raceFolder.frontMatter

    const date = DateTime.fromRFC2822(dateString.toString())
    const href = ['file', 'csv', raceGroup[0].slug].join('/')
    const content = `The ${year} ${name} has been run and won. Check out the  <a href=${href}>Results</a>`

    return {
      id: href,
      date: date.toISO(),
      title: { rendered: `${year} ${name}` },
      slug: href,
      content: { rendered: content },
      excerpt: { rendered: content },
      _embedded: {
        author: [
          {
            name: 'Andrew Creer',
            id: 'mate',
          },
        ],
        //'wp:featuredmedia': [],
      },
    }
  })
}

/*
;(async () => {
  console.log(chalk.blue(`Gday mate`))
  const allResultsGroups: BuacCSV[][][][] = require('../../../tmp-json/allResultByYearRaceLength.json')
  const resultPosts = resultsToPosts(allResultsGroups)
  writeFileSync(
    './tmp-json/allPostDelete.json',
    JSON.stringify(resultPosts, null, ' ')
  )
  console.log(resultPosts)
})()
*/
