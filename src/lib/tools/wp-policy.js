/* eslint-disable camelcase */
const fs = require('fs')
const path = require('path')
const download = require('download')

const allPolicy = [
  //'https://broken.com/goose',
  'https://bendigouniathsclub.org.au/blog/wp-content/uploads/2021/03/BUACOperatingProceduresandPoliciesV2.pdf',
  'https://bendigouniathsclub.org.au/blog/wp-content/uploads/2021/03/BUACCommunicationsPlan.pdf',
  'https://bendigouniathsclub.org.au/blog/wp-content/uploads/2021/03/BUACConstitution.pdf',
  'https://bendigouniathsclub.org.au/blog/wp-content/uploads/2021/03/BUACInclusionPolicy.pdf',
  'https://bendigouniathsclub.org.au/blog/wp-content/uploads/2021/03/BUACLifeMemberGuidelines.pdf',
  'https://bendigouniathsclub.org.au/blog/wp-content/uploads/2021/03/BUACMemberHandbook.pdf',
  'https://bendigouniathsclub.org.au/blog/wp-content/uploads/2021/03/BUACOperatingProceduresandPoliciesV2.pdf',
  'https://bendigouniathsclub.org.au/blog/wp-content/uploads/2021/03/BUACVolunteerGuidelines.pdf',
]

// git ignored folder we save pdfs to
const wpCache = path.join('.', 'public', 'assets', 'from-wordpress', 'policy')

const ensure = (folder) => {
  if (!fs.existsSync(folder)) {
    fs.mkdirSync(folder, { recursive: true })
  }
  return folder
}

;(async () => {
  const directory = ensure(wpCache)

  console.log(`Getting ${allPolicy.length} policy pages to ${directory}`)
  const downloads = (
    await Promise.all(
      allPolicy.map(async (url) => {
        console.log(`Getting ${url}`)
        const fn = path.join(directory, path.basename(url))
        const promise = await download(url, directory).catch((e) =>
          console.log('WHooops', e)
        )
        return { promise, fn, url }
      })
    ).catch((e) => {})
  ).filter(({ promise }) => promise)
  downloads.forEach(({ fn }) => {
    console.log(`const ${path.basename(fn, '.pdf')}=require('${fn}')`)
  })
})()
