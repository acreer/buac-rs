import {
  extendTheme,
  // Theme
} from '@chakra-ui/react'

/*
interface StyleOptions {
  theme: Theme
  colorMode: 'light' | 'dark'
  colorScheme: string
}
*/

// https://github.com/chakra-ui/chakra-ui/blob/master/packages/theme/src/components/button.ts
// type Dict = Record<string, any>

// import { mode } from '@chakra-ui/theme-tools'
export default extendTheme({
  initialColorMode: 'dark',
  useSystemColorMode: false,
  styles: {
    global: {
      a: {
        color: 'blue.500',
        _hover: {
          textDecoration: 'underline',
        },
      },
    },
    /*  Some issue with specificity
    global: (props) => ({
      a: {
        color: mode('yellow.500', 'blue.500')(props),
        hover: {
          textDecoration: 'underline',
        },
      },
    }),
    */
  },
  colors: {
    // https://smart-swatch.netlify.app/#fedd2c
    // where fedd2c is uniYellow
    yellow: {
      50: '#fffbdb',
      100: '#fff3ad',
      200: '#feeb7d',
      300: '#fee24b',
      400: '#feda1b',
      500: '#fedd2c',
      // 500: '#e4c101',
      600: '#b29600',
      700: '#7f6b00',
      800: '#4c4000',
      900: '#1b1500',
    },
  },
  components: {
    Button: {
      defaultProps: {
        colorScheme: 'yellow',
        size: 'sm',
      },
    },
  },
})
