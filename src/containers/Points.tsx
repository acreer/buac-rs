import React from 'react'
import { useRouteData } from 'react-static'
import { BuacCSV } from 'lib/race-folder'
import { BuacTypes } from 'lib/enums'

// Present
import Layout from 'components/Layout'
import {
  Button,
  Table,
  Tbody,
  Td,
  Text,
  Tr,
  Wrap,
  WrapItem,
  //Box,
} from '@chakra-ui/react'

import { Link } from 'components/Link'

type Data = {
  pointsByYearByLength: BuacCSV[][][]
  buacType: string
}
import { startCase } from 'lodash'
import CSVChooser from 'components/CSVChooser'

export const PointsButton = ({ buacType }: { buacType: string }) => {
  if (buacType == BuacTypes.speedPoints)
    return (
      <Link to="/results/speed">
        <Button size="xs">Speed Points</Button>
      </Link>
    )
  if (buacType == BuacTypes.clubPoints)
    return (
      <Link to="/results/club">
        <Button size="xs">Club Points</Button>
      </Link>
    )
  return null
}

export default () => {
  const { pointsByYearByLength, buacType }: Data = useRouteData()
  return (
    <Layout title={startCase(buacType.toLowerCase())}>
      <PointsButton
        buacType={
          buacType == BuacTypes.speedPoints
            ? BuacTypes.clubPoints
            : BuacTypes.speedPoints
        }
      />
      <Table>
        <Tbody size="sm">
          {pointsByYearByLength.map((lengths) => {
            const pointsCSV = lengths[0][0]
            return (
              <Tr key={pointsCSV.slug}>
                <Td>
                  <Text>
                    {pointsCSV.raceFolder.year} (
                    {pointsCSV.raceFolder.frontMatter.name})
                  </Text>
                </Td>
                <Td>
                  <Wrap>
                    {lengths.map((lengthGroup) => (
                      <WrapItem key={lengthGroup[0].buacLength}>
                        <CSVChooser csvs={lengthGroup} />
                      </WrapItem>
                    ))}
                  </Wrap>
                </Td>
              </Tr>
            )
          })}
        </Tbody>
      </Table>
    </Layout>
  )
}
