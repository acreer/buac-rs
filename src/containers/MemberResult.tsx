import React from 'react'
import { useRouteData, Head } from 'react-static'
import {
  Heading,
  Text,
  Wrap,
  WrapItem,
  Table,
  Tbody,
  Tr,
  Td,
} from '@chakra-ui/react'

import { BuacCSV } from 'lib/race-folder'
import Layout from 'components/Layout'
import CSVChooser from 'components/CSVChooser'
import { BuacMember } from 'lib/members'

type Data = {
  member: BuacMember
  csvs: BuacCSV[][][][]
}

export default () => {
  const { member, csvs }: Data = useRouteData()
  const title = `${member.name} - ${member.club}`

  return (
    <Layout>
      <Head>
        <title>{title}</title>
      </Head>
      <Heading>{title}</Heading>
      <Table size="sm">
        <Tbody>
          {csvs.map((yearGroup) => (
            <React.Fragment key={yearGroup[0][0][0].fn}>
              <Tr>
                <Td colSpan={2}>
                  <Heading>{yearGroup[0][0][0].raceFolder.year}</Heading>
                </Td>
              </Tr>
              {yearGroup.map((raceGroup) => (
                <Tr key={raceGroup[0][0].fn}>
                  <Td>
                    <Text>{raceGroup[0][0].raceFolder.frontMatter.name}</Text>
                  </Td>
                  <Td>
                    <Wrap>
                      {raceGroup.map((lengthGroup) => (
                        <WrapItem key={lengthGroup[0].buacLength}>
                          <CSVChooser csvs={lengthGroup} />
                        </WrapItem>
                      ))}
                    </Wrap>
                  </Td>
                </Tr>
              ))}
            </React.Fragment>
          ))}
        </Tbody>
      </Table>
    </Layout>
  )
}
/**
 * */
