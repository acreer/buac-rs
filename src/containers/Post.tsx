import React from 'react'
import { useRouteData, Head } from 'react-static'
import Layout from '../components/Layout'
import { Button, HStack, Box, Image, Stack } from '@chakra-ui/react'
// import { WpPost } from '../../types/index'
import { WP_REST_API_Post, WP_REST_API_Media } from 'wp-types'
import { Link } from 'components/Link'
import { AuthorDate, PostHeading } from 'pages/news'
import { DangerousBox } from 'components/Chakra'

type PostButtonProps = {
  nextSlug: string
  prevSlug: string
  newsSlug: string
}
const PostButtons = ({ nextSlug, prevSlug, newsSlug }: PostButtonProps) => (
  <HStack>
    <Link to={`/news/post/${prevSlug}`}>
      <Button disabled={prevSlug.length == 0}>Previous</Button>
    </Link>
    <Link to={newsSlug}>
      <Button>News</Button>
    </Link>
    <Link to={`/news/post/${nextSlug}`}>
      <Button disabled={nextSlug.length == 0}>Next</Button>
    </Link>
  </HStack>
)
type Data = {
  post: WP_REST_API_Post
} & PostButtonProps

// eslint-disable-next-line react/display-name
export default () => {
  const { post, nextSlug, prevSlug, newsSlug }: Data = useRouteData()
  const featuredMedias: WP_REST_API_Media[] = post._embedded[
    'wp:featuredmedia'
  ] as WP_REST_API_Media[]
  const featured = featuredMedias ? (
    <Image
      boxSize="400px"
      objectFit="contain"
      src={featuredMedias[0].source_url}
    />
  ) : null

  return (
    <Layout>
      <Head>
        <title dangerouslySetInnerHTML={{ __html: post.title.rendered }} />
      </Head>
      <Box p="4">
        <PostButtons {...{ nextSlug, prevSlug, newsSlug }} />
        <Box my="2">
          <PostHeading post={post} />
        </Box>
        <AuthorDate post={post} />
        <Stack my="5" direction={['column-reverse', 'row']}>
          <DangerousBox
            dangerouslySetInnerHTML={{ __html: post.content.rendered }}
          />
          {featured}
        </Stack>
        <PostButtons {...{ nextSlug, prevSlug, newsSlug }} />
      </Box>
    </Layout>
  )
}
