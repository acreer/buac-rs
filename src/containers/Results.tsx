import React from 'react'
import { useRouteData } from 'react-static'
import { Link } from 'react-router-dom'
import { Link as YellowLink } from 'components/Link'
import { csvDateTime, defDateFormat } from 'pages/runs'
import { startCase } from 'lodash'
import {
  Button,
  Table,
  Tbody,
  Td,
  Text,
  Tr,
  Wrap,
  WrapItem,
  Box,
} from '@chakra-ui/react'
import { BuacTypes } from 'lib/enums'
import { BuacCSV } from 'lib/race-folder'
//import { csvTitle } from './CSV'
import Layout from 'components/Layout'
import CSVChooser from 'components/CSVChooser'

import { PointsButton } from 'containers/Points'

type Data = {
  year: string
  years: string[]
  raceGroupsByLength: BuacCSV[][][]
}

export default () => {
  const { year, years, raceGroupsByLength }: Data = useRouteData()
  const bType = raceGroupsByLength[0][0][0].buacType
  const title = `${startCase(bType.toLowerCase())}s ${year}`
  const linkBase = `${bType.toLowerCase()}s`
  return (
    <Layout title={title}>
      {bType == BuacTypes.result && (
        <Box py={1}>
          <YellowLink to={`/handicaps/${year}`}>
            <Button size="xs">{year} Handicaps</Button>
          </YellowLink>
          <PointsButton buacType={BuacTypes.clubPoints} />
          <PointsButton buacType={BuacTypes.speedPoints} />
        </Box>
      )}

      {years.map((y) => (
        <Button size="sm" disabled={y == year} key={y}>
          <Link to={`/${linkBase}/${y}`}>{y}</Link>
        </Button>
      ))}

      <Table size="sm">
        <Tbody>
          {raceGroupsByLength.map((raceGroup) => {
            const raceCSV = raceGroup[0][0]
            return (
              <Tr key={raceCSV.slug}>
                <Td>
                  <Text size="md">{raceCSV.raceFolder.frontMatter.name} </Text>
                  <Text size="sm">
                    {csvDateTime(raceCSV).toFormat(defDateFormat)}
                  </Text>
                </Td>
                <Td>
                  <Wrap>
                    {raceGroup.map((lengthGroup) => (
                      <WrapItem key={lengthGroup[0].buacLength}>
                        <CSVChooser csvs={lengthGroup} />
                      </WrapItem>
                    ))}
                  </Wrap>
                </Td>
              </Tr>
            )
          })}
        </Tbody>
      </Table>
    </Layout>
  )
}
