import React from 'react'
import { Head, useRouteData } from 'react-static'
import { Box, Heading, Text, Stack, Flex } from '@chakra-ui/react'
import { kebabCase } from 'lodash'
import { Link } from 'components/Link'

import { RunMarkdown } from 'types/index'
import Layout from 'components/Layout'
import { mdRaceColor, raceDateFormat } from 'pages/runs'
import MarkdownLink from '../components/MarkdownLink'
import { buacCSVSlug } from 'components/CSVChooser'

import { RaceWinner, WinnerRow } from 'lib/tools/to-json'
import { BorderBox } from 'components/Chakra'
import { BuacCarousel } from 'components/Carousel'

const WinnerCellInfo = ({
  info,
  text,
}: {
  info: WinnerRow | undefined
  text: string
}) => {
  if (!info) return null
  // See similar in SortableTablee.tsx
  const name = info.name ? info.name : `${info.first} ${info.last}`
  const time = info.time || info.netTime
  const memberSlug = kebabCase(name)

  return (
    <Stack borderTop="1px dotted gray" spacing={2} direction={['row', 'row']}>
      <Box py={1}>{text}</Box>
      <Box py={1}>
        <Link to={`/person/${memberSlug}`}>{name}</Link>
      </Box>
      <Box py={1}>{time}</Box>
    </Stack>
  )
}

type WinnerCellProps = {
  winner: RaceWinner | undefined
  year?: string
}

const WinnerCell = ({ winner, year }: WinnerCellProps) => {
  if (!winner) return null
  return (
    <BorderBox>
      <Box py={2}>
        <Link to={buacCSVSlug(winner.csv)}>
          {year} {winner.csv.csvType} {winner.csv.buacLength}{' '}
        </Link>
      </Box>
      <WinnerCellInfo info={winner.clubWinner} text="Club Handicap" />
      <WinnerCellInfo info={winner.fastestFemale} text="Fastest Female" />
      <WinnerCellInfo info={winner.fastestMale} text="Fastest Male" />
    </BorderBox>
  )
}

type Data = {
  race: RunMarkdown
  winners: RaceWinner[][]
}

// eslint-disable-next-line react/display-name
export default () => {
  const { race, winners }: Data = useRouteData()
  const hasImages = race.markdown.images && race.markdown.images.length > 0
  const hasMaps = race.markdown.iframes && race.markdown.iframes.length > 0
  return (
    <Layout noCarousel={hasImages}>
      <Head>
        <title>{race.markdown.title}</title>
      </Head>
      {hasImages && (
        <BuacCarousel>
          {race.markdown.images.map((src) => (
            <img key={src} src={src} />
          ))}
        </BuacCarousel>
      )}
      <Heading color={mdRaceColor(race)}>{race.markdown.title}</Heading>
      <Link to="/runs">All Events</Link>
      <Text>{raceDateFormat(race, 'DDDD')}</Text>
      <Text>
        Setter: <MarkdownLink link={race.markdown.setter} />
      </Text>
      <Text>
        Location: <MarkdownLink link={race.markdown.location} />
      </Text>
      <Text>Distances {race.markdown.distances}</Text>

      <div
        style={{ marginTop: '25px' }}
        dangerouslySetInnerHTML={{ __html: race.markdown.contents }}
      />
      {
        // https://jameshfisher.com/2017/08/30/how-do-i-make-a-full-width-iframe/
        hasMaps && (
          <Flex wrap="wrap">
            {race.markdown.iframes.map(({ src, title, padding = 56.25 }) => (
              <Box px={[0, 2]} minW={['xs', 'md', 'xl']} key={src}>
                <Heading size="sm">{title}</Heading>
                <Box position="relative" paddingTop={`${padding}%`}>
                  <iframe
                    src={src}
                    frameBorder="0"
                    allowFullScreen
                    style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                    }}
                  ></iframe>
                </Box>
              </Box>
            ))}
          </Flex>
        )
      }

      {winners && (
        <React.Fragment>
          <Heading marginTop={5} size="md">
            Fastest Times
          </Heading>
          {winners.map((yrLengths: RaceWinner[], yix: number) => {
            const year = `${yrLengths[0].csv.raceFolder.year}`
            return (
              <BorderBox key={yix} borderColor="yellow.500">
                <Heading size="sm">{year}</Heading>
                <Flex wrap="wrap">
                  {yrLengths.map((w) => (
                    <WinnerCell key={w.csv.slug} winner={w} year={year} />
                  ))}
                </Flex>
              </BorderBox>
            )
          })}
        </React.Fragment>
      )}
    </Layout>
  )
}
