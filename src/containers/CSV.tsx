/* eslint-disable react/display-name */
import React from 'react'
import { BuacCSV } from 'lib/race-folder'
import { useRouteData } from 'react-static'
import Layout from 'components/Layout'
import {
  Button,
  Wrap,
  WrapItem,
  Text,
  Flex,
  Spacer,
  Box,
  Collapse,
  useDisclosure,
} from '@chakra-ui/react'
import { startCase, map } from 'lodash'
import SortableTable from 'components/SortableTable'
import CSVChooser from 'components/CSVChooser'
import { csvDateTime, defDateFormat } from 'pages/runs'

import { Link } from 'components/Link'

import { PointsButton } from 'containers/Points'
import { BuacTypes } from 'lib/enums'

export const csvTitle = (csv: BuacCSV) =>
  [
    csv.raceFolder.year,
    csv.raceFolder.frontMatter.name,
    csv.buacLength.match(/\d\./)
      ? csv.buacLength
      : startCase(csv.buacLength.toLowerCase()),
    csv.csvType,
    startCase(csv.buacType.toLowerCase()),
  ]
    .filter((s: string) => s.length)
    .join(' ')

type Data = {
  csv: BuacCSV
  typeLengthGroups: BuacCSV[][]
  otherTypeLengthGroups: Record<string, BuacCSV[][]>
}
const Chooser = ({
  csv,
  typeLengthGroups,
}: Pick<Data, 'csv' | 'typeLengthGroups'>) => (
  <Wrap>
    {typeLengthGroups.map((typeLengthGroup) => (
      <WrapItem key={typeLengthGroup[0].buacLength}>
        <CSVChooser csvs={typeLengthGroup} activeCSV={csv} />
      </WrapItem>
    ))}
  </Wrap>
)

export default () => {
  const { csv, typeLengthGroups, otherTypeLengthGroups }: Data = useRouteData()
  const title = csvTitle(csv)
  const { isOpen, onToggle, onClose } = useDisclosure()
  return (
    <Layout title={title}>
      <Flex>
        <Text>{csvDateTime(csv).toFormat(defDateFormat)}</Text>
        <Spacer />
        {[
          BuacTypes.clubPoints.toString(),
          BuacTypes.speedPoints.toString(),
        ].includes(csv.buacType) && (
          <Box px={2}>
            <PointsButton buacType={BuacTypes.speedPoints.toString()} />
            <PointsButton buacType={BuacTypes.clubPoints.toString()} />
          </Box>
        )}
        {Object.keys(otherTypeLengthGroups).length == 1 ? (
          <Link to={Object.values(otherTypeLengthGroups)[0][0][0].slug}>
            <Button size="xs" mx={3}>
              {startCase(
                Object.values(
                  otherTypeLengthGroups
                )[0][0][0].buacType.toLowerCase()
              )}
            </Button>
          </Link>
        ) : (
          <React.Fragment>
            <Collapse in={isOpen}>
              <Wrap spacing={[0, '30px']}>
                {map(otherTypeLengthGroups, (lengthGroups, bType) => (
                  <WrapItem key={bType}>
                    <Box>
                      <Link to={lengthGroups[0][0].slug} onClick={onClose}>
                        {startCase(bType.toLowerCase())}
                      </Link>
                    </Box>
                  </WrapItem>
                ))}
              </Wrap>
            </Collapse>
            {Object.keys(otherTypeLengthGroups).length > 0 && (
              <Button mx={'6'} size="xs" onClick={onToggle}>
                Other Race Data
              </Button>
            )}
          </React.Fragment>
        )}
        <Text>{csv.rows.length} runners</Text>
      </Flex>
      <Box py="1.5"></Box>
      <Chooser {...{ csv, typeLengthGroups }} />
      <SortableTable data={csv.rows} />
      <Chooser {...{ csv, typeLengthGroups }} />
    </Layout>
  )
}
