# Bendigo University Athletics Club

This is the source code for https://bendigouniathsclub.org.au/rs/

## Overview

There are various source of data

- News articles and image come from wordpres site https://bendigouniathsclub.org.au/blog
- Race results live in ()[./src/data/race-data]
-

## Getting started

- Clone this repo

```
cd buac-rs
yarn
yarn prepare:wordpress
yarn prepare:json
yarn start
```

browse https://localhost:3000/rs
